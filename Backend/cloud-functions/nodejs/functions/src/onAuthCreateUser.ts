import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();

export const onAuthCreateUser = functions.auth.user().onCreate((user) => {
    const uid = user.uid;
    const email = user.email;
    const tempName = email?.split('@')[0];
    let profilePic = user.photoURL;
    //if no profile picture, then set it to empty text
    if (profilePic === undefined || profilePic === null) profilePic = '';

    //create new user child in users root parent for this user
    return db.ref('users').child(uid).set({
        uid: uid,
        createdAt: admin.database.ServerValue.TIMESTAMP,
        userInfo: {
            name: tempName,
            email: email,
            profilePic: profilePic
        }
    }).then(() => {
        //DONE! new child for the signed in user was created in users root parent in the db
        console.log('DONE: New user with uid: ' + uid + ' was successfully added to users root node!');
    }).catch(err => {
        console.log('Field to add a user with uid: ' + uid + ' to users root node!', err);
    });
});