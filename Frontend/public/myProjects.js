//========= variables =========//
let docsRef;
let usersRef;
let templateRef;
let templatesObject;
//====== invoke on load ======//
(function () {
    var firebaseConfig = {
        apiKey: "AIzaSyCaaBXjrgPKRry7SU2CqD9MQ-PAuWnvoi4",
        authDomain: "docbook-2021.firebaseapp.com",
        databaseURL: "https://docbook-2021-default-rtdb.firebaseio.com",
        projectId: "docbook-2021",
        storageBucket: "docbook-2021.appspot.com",
        messagingSenderId: "438833982715",
        appId: "1:438833982715:web:0bc7de4c296d4bea5cd45e",
        measurementId: "G-S5NMEZDF65"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    docsRef = firebase.database().ref("documents");
    usersRef = firebase.database().ref("users");
    templateRef = firebase.database().ref("templates");

    //get my user child with all documents in it
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        //for bar username and profile pic
        const profilePic = user.photoURL;
        //set username
        $('#username-bar').text(user.displayName);
        //set profilePic
        if (profilePic !== undefined && profilePic !== null && profilePic !== "") {
            //manipulate
            $("#profilePic-bar").attr("src", "");
            $("#profilePic-bar").attr("src", profilePic);
            document.getElementById("profilePic-bar").style.height = '100%';
            document.getElementById("profilePic-bar").style.width = '100%';
        }

        //listen to document change (add, modify or remove) from my sharedDocs child
        usersRef.child(user.uid).child("sharedDocs").on('value', (sharedDocsSnap) => {
            document.getElementById("projectsCards").innerHTML = "";
            const sharedDocs = sharedDocsSnap.val();
            if (sharedDocs === null || sharedDocs === undefined) return;
            for (let docId of Object.keys(sharedDocs)) {
                const docProject = {
                    id: docId,
                    title: sharedDocs[docId].title,
                    role: sharedDocs[docId].role
                };
                createCard(docProject);
            }
          });

        //get templates
        templateRef.get().then(templatesSnaps => {
            //get all temples titles
            templatesObject = templatesSnaps.val();
            const templesKeys = Object.keys(templatesObject);
            for (let templateTitle of templesKeys) {
                const templateOption = $("<option value=\"" + templateTitle + "\">" + templateTitle + "</option>");
                templateOption.appendTo('#temples-select');
            }

            //now we can listen to create new project button click
            document.getElementById("create-new-project").addEventListener("click", createNewDoc);
        }).catch(err => {
            console.log("Failed to get templates! " + err);
        });

        //listen to file chooser
        const uploadXmlInput = document.getElementById("upload-xml-input");
        uploadXmlInput.addEventListener("change", handleFiles, false);
    });
})();

//========================= on create new document button press =========================//
function createNewDoc() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        const docTitle = document.getElementById("newDocTitle").value.trim();
        document.getElementById("errorLabel-creatDoc").style.visibility = "hidden";
        if (docTitle === undefined || docTitle === null || docTitle === "") {
            document.getElementById("errorLabel-creatDoc").style.visibility = "visible";
            return;
        }

        //get template content and other doc details
        const selTemplateKey = $('#temples-select').find(":selected").text();
        templatesObject.None = ""; //add none to templates
        //init document info
        const docProject = {
            id: docsRef.push().key,
            title: docTitle,
            role: "Owner",
            templateContent: templatesObject[selTemplateKey]
        };
        const timeNow = firebase.database.ServerValue.TIMESTAMP;

        //add new project to firebase
        docsRef.child(docProject.id).child("metadata").set({
            docId: docProject.id,
            title: docProject.title,
            xmlData: docProject.templateContent,
            htmlData: "",
            createdAt: timeNow,
            createdBy: user.uid,
            xmlUpdatedAt: timeNow,
            xmlUpdatedBy: user.uid
        }).then(() => {
            $('#newProject').modal('hide');
            document.getElementById("newDocTitle").value = "";
        }).catch(err => {
            console.log("Failed to save doc!");
        });
    });
}

//when click on upload xml, pop up a file chooser window
function uploadXmlBtn() { $('#upload-xml-input').trigger('click'); }

//when the user clicks open xml on the file chooser
function handleFiles() {
    const fileList = this.files;
    if (FileReader && fileList && fileList.length) {
        uploadedFile = fileList[0];
        const fr = new FileReader();
        fr.onload = function () {
            //init document info
            const docProject = {
                id: docsRef.push().key,
                title: uploadedFile.name.slice(0, -4),
                role: "Owner",
                xmlContent: fr.result
            };
            //create new doc from xml
            createNewDocFromXml(docProject);
        }
        fr.readAsText(uploadedFile);
    }
}

//========================= Create new doc from xml =========================//
function createNewDocFromXml(docProject) {
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        const timeNow = firebase.database.ServerValue.TIMESTAMP;

        //add new project to firebase
        docsRef.child(docProject.id).child("metadata").set({
            docId: docProject.id,
            title: docProject.title,
            xmlData: docProject.xmlContent,
            htmlData: "",
            createdAt: timeNow,
            createdBy: user.uid,
            xmlUpdatedAt: timeNow,
            xmlUpdatedBy: user.uid
        }).then(() => {
            $('#newProject').modal('hide');
            document.getElementById("newDocTitle").value = "";
        }).catch(err => {
            console.log("Failed to save doc!");
        });
    });
}

function logout() {
    firebase.auth().signOut().then(() => {
        console.log("User logged out!");
        $('#logoutModal').modal('hide');
        window.location.replace("index.html");
    }).catch((error) => {
        console.log(error);
    });
}

function createCard(docProject) {
    const cardDiv = $("<div class=\"dropdown-box-modal\">\n" +
        "                        <div class=\"document-box\">\n" +
        "                            <h2 class=\"text-blue\">" + docProject.title + "</h2>\n" +
        "                            <p lass=\"text-blue\">" + docProject.role + "</p>\n" +
        "                            <div class=\"proj-btn d-flex justify-content-between align-items-center\">\n" +
        "                                <div class=\"div\">\n" +
        "                                    <a href=\"editor.html#" + docProject.id + "\" type=\"button\" class=\"btn btn-primary modal-btn hover-btn\">\n" +
        "                                        Open\n" +
        "                                    </a>\n" +
        "                                </div>\n" +
        "                                <div style=\"display:none\" class=\"dropdown\">\n" +
        "                                    <button type=\"button\" class=\"btn btn-primary butn-icon\" data-bs-toggle=\"dropdown\"\n" +
        "                                        aria-expanded=\"false\">\n" +
        "                                        <img src=\"assets/icons/btn-icon.svg\">\n" +
        "                                    </button>\n" +
        "                                    <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton1\">\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item\" type=\"button\" data-bs-toggle=\"modal\"\n" +
        "                                                data-bs-target=\"#inviteModal\">\n" +
        "                                                <img src=\"assets/icons/share.svg\" alt=\"\" class=\"icons\"> Invite User\n" +
        "                                            </a>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item\" id=\"userPermitBtn\" type=\"button\"\n" +
        "                                                data-bs-toggle=\"modal\" data-bs-target=\"#userPermit\">\n" +
        "                                                <img src=\"assets/icons/ic_permissions.svg\" alt=\"\" class=\"icons\"> User\n" +
        "                                                and Permissions</a>\n" +
        "                                        </li>\n" +
        "                                        <li class=\"downloaddrop\">\n" +
        "                                            <a class=\"dropdown-item dropdown-toggle\">\n" +
        "                                                <img src=\"assets/icons/ic_download.svg\" alt=\"\" class=\"icons\"> Download\n" +
        "                                            </a>\n" +
        "                                            <ul class=\"dropdown-menu\">\n" +
        "                                                <li>\n" +
        "                                                    <a href=\"#\" class=\"dropdown-item\">\n" +
        "                                                        <img src=\"assets/icons/ic_xml_file.svg\" alt=\"\" class=\"icons\">\n" +
        "                                                        Download XML File\n" +
        "                                                    </a>\n" +
        "                                                </li>\n" +
        "                                                <li>\n" +
        "                                                    <a href=\"#\" class=\"dropdown-item\">\n" +
        "                                                        <img src=\"assets/icons/ic_html_file.svg\" alt=\"\" class=\"icons\">\n" +
        "                                                        Download HTML File\n" +
        "                                                    </a>\n" +
        "                                                </li>\n" +
        "                                                <li>\n" +
        "                                                    <a href=\"#\" class=\"dropdown-item\">\n" +
        "                                                        <img src=\"assets/icons/ic_pdf_file.svg\" alt=\"\" class=\"icons\">\n" +
        "                                                        Generate PDF Document\n" +
        "                                                    </a>\n" +
        "                                                </li>\n" +
        "                                            </ul>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <hr class=\"dropdown-divider\">\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item\" href=\"#\">\n" +
        "                                                <img src=\"assets/icons/ic_rename.svg\" alt=\"\" class=\"icons\"> Rename\n" +
        "                                                Document\n" +
        "                                            </a>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item txt-red\" id=\"removeBtn\" type=\"button\"\n" +
        "                                                data-bs-toggle=\"modal\" data-bs-target=\"#removeForUser\">\n" +
        "                                                <img src=\"assets/icons/remove-red.svg\" alt=\"\" class=\"icons\"> Remove\n" +
        "                                                Document\n" +
        "                                            </a>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item txt-red\" id=\"#leavedoc\" type=\"button\"\n" +
        "                                                data-bs-toggle=\"modal\" data-bs-target=\"#leavedoc\">\n" +
        "                                                <img src=\"assets/icons/logout-red.svg\" alt=\"\" class=\"icons\"> Leave\n" +
        "                                                Document\n" +
        "                                            </a>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item\" type=\"button\" data-bs-toggle=\"modal\"\n" +
        "                                                data-bs-target=\"#makeCopy\">\n" +
        "                                                <img src=\"assets/icons/ic_content_copy.svg\" alt=\"\" class=\"icons\"> Make a\n" +
        "                                                Copy\n" +
        "                                            </a>\n" +
        "                                        </li>\n" +
        "                                        <li>\n" +
        "                                            <a class=\"dropdown-item\" type=\"button\" data-bs-toggle=\"modal\"\n" +
        "                                                data-bs-target=\"#docDetail\">\n" +
        "                                                <img src=\"assets/icons/ic_details.svg\" alt=\"\" class=\"icons\"> Document\n" +
        "                                                Detail</a>\n" +
        "                                        </li>\n" +
        "                                    </ul>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>");
    cardDiv.prependTo('#projectsCards');
}
