import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();
const OWNER = 'Owner';

export const onCreateDoc = functions.database.ref('/documents/{docId}')
    .onCreate(async (snapshot, context) => {
        //auth validation
        if (context.auth === undefined || context.auth === null) {
            console.log('The function must be called while authenticated!');
            return null;
        }
        const myUid = context.auth.uid;
        const docId = context.params.docId;

        //============================= get my userInfo node and catch errors =============================//
        let myUserInfo: any;
        try {
            myUserInfo = (await db.ref('users').child(myUid).child('userInfo').get()).val();
        }
        catch (err) {
            const msg = 'Failed to get user child who created the doc, uid: ' + myUid;
            console.log(msg, err);
            return null;
        }

        //validation: check if userInfo node is not found
        if (myUserInfo === undefined || myUserInfo === null) {
            const msg = 'Failed to get userInfo child of the user who created the doc, uid: ' + myUid;
            console.log(msg);
            return null;
        }

        //====================== put myself in users child in documents root parent ======================//
        try {
            await db.ref('documents').child(docId).child('users').child(myUid).set({
                uid: myUid,
                name: myUserInfo.name,
                email: myUserInfo.email,
                profilePic: myUserInfo.profilePic,
                role: OWNER
            });
        }
        catch (err) {
            const msg = 'Failed to save user child to (documents/users), uid: '
                + myUid + ', docId: ' + docId;
            console.log(msg, err);
            return null;
        }

        //========== save document in my user node inside users root parent in sharedDocs child ==========//
        return db.ref('users').child(myUid).child('sharedDocs').child(docId).set({
            docId: docId,
            title: snapshot.child('metadata/title').val(),
            role: OWNER
        }).then(() => {
            //DONE!
            console.log('DONE: Created document successfully! docId: ' + docId + ', uid: ' + myUid);
        }).catch(err => {
            const message = 'Failed to save doc child to (users/sharedDocs), uid: '
                + myUid + ', docId: ' + docId;
            console.log(message, err);
        });
    });