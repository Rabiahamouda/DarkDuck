import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();
const GENERAL_ERR_MSG = 'Something went wrong!';
//======== roles ========//
const OWNER = 'Owner';
const EDITOR = 'Editor';
const VIEWER = 'Viewer';

export const inviteUser = functions.https.onCall(async (data, context) => {
    //For inviting users to a document, you need these arguments:
    /*arguments:
    - docId: string
    - invitedEmail: string
    - invitedRole: string in [Owner, Editor, Viewer]
    */
    const docId = data.docId;
    const invitedEmail = data.invitedEmail;
    const invitedRole = data.invitedRole;

    //============================= auth and argument validations =============================//
    //arguments validation
    if (docId === null || invitedEmail === null ||
        docId === undefined || invitedEmail === undefined
        || (invitedRole !== EDITOR && invitedRole !== OWNER && invitedRole !== VIEWER)) {
        const detailedMsg = 'One or more invalid arguments!'
        console.log(detailedMsg)
        throw new functions.https.HttpsError('invalid-argument', GENERAL_ERR_MSG, detailedMsg);
    }

    //auth validation
    if (context.auth === undefined || context.auth === null) {
        const detailedMsg = 'The function must be called only if the invoker is authenticated!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition', GENERAL_ERR_MSG, detailedMsg);
    }

    //============================= validation for the invited user =============================//
    //get the id of the invited user by his email, and throw an exception if he doesn't exist
    let invitedUid: any;
    try {
        invitedUid = (await admin.auth().getUserByEmail(invitedEmail)).uid;
    } catch (err) {
        const detailedMsg = 'User with email: ' + invitedEmail + ', doesn\'t exist!';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('failed-precondition', detailedMsg, detailedMsg);
    }

    /*get the invited user snapshot from path (documents/docId/users)
      to check if he is not already a member in this doc*/
    let memberSnapshot: any;
    try {
        memberSnapshot = (await db.ref('documents').child(docId)
            .child('users').child(invitedUid).get()).val();
    }
    catch (err) {
        const detailedMsg = 'Error fetching users parent within document with id: ' + docId;
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //validation: check if the user is already a member in the doc
    if (memberSnapshot !== undefined && memberSnapshot !== null) {
        const detailedMsg = 'The invited user with uid: ' + invitedUid +
            ', is already a member in docId: ' + docId;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('already-exists',
            'This user is already a member in this document!', detailedMsg);
    }

    //get the data of the invited user by his uid, and catch internal error if exists
    let invitedUserSnapshot: any;
    try {
        invitedUserSnapshot = (await db.ref('users').child(invitedUid).get()).val();
    }
    catch (err) {
        const detailedMsg = 'Error fetching the data of user with uid: '
            + invitedUid + ' from users root parent!';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    /*validation: if the user doesn't exist in users root parent, throw an exception!
      Note: usually if the system works well, it will never enter here!
      Because if the email of the invited user exists (in pervious validation),
      then the user must have a child in users root parent.
    */
    if (invitedUserSnapshot === undefined || invitedUserSnapshot === null) {
        const detailedMsg = 'User with uid: ' + invitedUid + ', doesn\'t exist!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //================================ validation for the inviter ================================//
    //get inviter's uid and role, if can't find, return!
    const myUid = context.auth?.uid;
    let myRole: any;
    try {
        myRole = (await db.ref('documents').child(docId).child('users')
            .child(myUid).child('role').get()).val();
    }
    catch (err) {
        const detailedMsg = 'The inviter is not a member in the doc, ' +
            +'or failed to get his role from (documents/' + docId + '/users/' + myUid + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found',
            'You are not a member in this document anymore!', detailedMsg);
    }

    //validation: if the inviter is not Editor or Owner, return
    if (myRole !== OWNER && myRole !== EDITOR) {
        const detailedMsg = 'You must be an owner or an editor to invite users! uid: '
            + myUid + ', the current role is: ' + myRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition',
            'You must be an owner/editor to invite users, but your role is: ' + myRole, detailedMsg);
    }

    //validation: if you are an editor and adding an owner, return
    if (myRole === EDITOR && invitedRole === OWNER) {
        const detailedMsg = 'You must be owner to invite owners! uid: ' + myUid;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition',
            'Only owners are allowed to remove owners, but your role is: ' + myRole, detailedMsg);
    }

    //============== finally after all validations, we can add the user in our doc ==============//
    return (await addUserToDoc(invitedUserSnapshot, docId, invitedRole));
});

//============================= Function: add user to the document =============================//
async function addUserToDoc(invitedUser: any, docId: string, role: string) {
    //add the member in (documents/docId/users) path
    try {
        await db.ref('documents').child(docId).child('users').child(invitedUser.uid).set({
            uid: invitedUser.uid,
            name: invitedUser.userInfo.name,
            email: invitedUser.userInfo.email,
            profilePic: invitedUser.userInfo.profilePic,
            role: role
        });
    }
    catch (err) {
        const detailedMsg = 'Failed to save user to (documents/'
            + docId + '/users/' + invitedUser.uid + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    //get document title to add it to sharedDocs
    let docTitle: any;
    try {
        docTitle = (await db.ref('documents').child(docId).child('metadata/title').get()).val();
    }
    catch (err) {
        const detailedMsg = 'Can\'t get document title, docId: ' + docId;
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //add the document inside (users/uid/sharedDocs/docId)
    return db.ref('users').child(invitedUser.uid).child('sharedDocs').child(docId).set({
        docId: docId,
        title: docTitle,
        role: role
    }).then(() => {
        //DONE!
        const detailedMsg = 'DONE: Successfully invited user with id '
            + invitedUser.uid + ', to docId: ' + docId + ', as a/an ' + role;
        console.log(detailedMsg);
        return {
            userRes: 'The user was successfully added to the document!',
            detailedRes: detailedMsg
        };
    }).catch(err => {
        const detailedMsg = 'Failed to save doc in path (users/' +
            invitedUser.uid + '/sharedDocs/' + docId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    });
}