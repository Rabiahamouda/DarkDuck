//for fop
gcloud functions deploy xmlToPdf --entry-point com.docbook.ConvertFOP --runtime java11 --memory 512MB --trigger-http

//for saxon
gcloud functions deploy xmlToHtml --entry-point com.docbook.SaxonTransform --runtime java11 --memory 512MB --trigger-event  providers/google.firebase.database/eventTypes/ref.write --trigger-resource projects/_/instances/docbook-2021-default-rtdb/refs/documents/{docId}/metadata/xmlData
