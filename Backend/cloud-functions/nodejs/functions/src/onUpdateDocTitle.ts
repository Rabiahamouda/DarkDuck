import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();

export const onUpdateDocTitle = functions.database.ref('/documents/{docId}/metadata/title')
    .onUpdate(async (snapshot, context) => {
        const newTitle = snapshot.after.val();
        const docId = context.params.docId;

        //get ids of users (members) of the document docId
        let usersOfDoc: any;
        try {
            usersOfDoc = await db.ref('documents').child(docId).child('users').get();
        }
        catch (err) {
            const detailedMsg = 'Failed to get members (users parent) of docId: ' + docId;
            console.log(detailedMsg, err);
            return null;
        }

        //get all members (users) ids
        const membersIds = Object.keys(usersOfDoc.val());
        //for saving update promises
        const updateTitlePromises = [];

        //loop through all sharedDocs child for each member of the document
        for (let userId of membersIds) {
            updateTitlePromises.push(db.ref('users').child(userId).child('sharedDocs')
                .child(docId).update({ title: newTitle }).catch(err => {
                    console.log('Failed to update doc node in (users/' + userId
                        + '/sharedDocs/' + docId + ')', err);
                }));
        }

        //return the result of all promises
        return Promise.all(updateTitlePromises).then(() => {
            //DONE!: updated all titles of docs in (users/sharedDocs)!
            console.log('DONE: Successfully updated all titles of docs in (users/sharedDocs)! DocId: ' + docId);
        }).catch(err => {
            console.log('Failed to update one or more doc in (users/sharedDocs), docId: ' + docId, err);
        });
    });