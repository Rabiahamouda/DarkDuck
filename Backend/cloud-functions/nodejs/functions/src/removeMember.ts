import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();
const GENERAL_ERR_MSG = 'Something went wrong!';
//======== roles ========//
const OWNER = 'Owner';
const EDITOR = 'Editor';
const VIEWER = 'Viewer';

export const removeMember = functions.https.onCall(async (data, context) => {
    //For removing member from a document, you need these arguments:
    /*arguments:
    - docId: string
    - memberEmail: string
    */
    const docId = data.docId;
    const memberEmail = data.memberEmail;

    //=================================== auth and argument validations ===================================//
    //arguments validation
    if (docId === null || memberEmail === null ||
        docId === undefined || memberEmail === undefined) {
        const detailedMsg = 'One or more invalid arguments!'
        console.log(detailedMsg)
        throw new functions.https.HttpsError('invalid-argument', GENERAL_ERR_MSG, detailedMsg);
    }

    //auth validation
    if (context.auth === undefined || context.auth === null) {
        const detailedMsg = 'The function must be called only if the invoker is authenticated!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition', GENERAL_ERR_MSG, detailedMsg);
    }

    //================================ validation of the user to be removed ================================//
    //get his uid and check if he exist in the system
    let memberId: any;
    try {
        memberId = (await admin.auth().getUserByEmail(memberEmail)).uid;
    } catch (err) {
        const detailedMsg = 'User with email: ' + memberEmail + ', doesn\'t exist!';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('failed-precondition', detailedMsg, detailedMsg);
    }

    //get his role and validate his membership in the document
    let memberRole: any;
    try {
        memberRole = (await db.ref('documents').child(docId).child('users')
            .child(memberId).child('role').get()).val();
    } catch (err) {
        const detailedMsg = 'The user who is about to be removed is not a member in the doc, ' +
            +'or failed to get his role from (documents/' + docId + '/users/' + memberId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found',
            'The member you are trying to remove is already removed from this doc!', detailedMsg);
    }

    //validation: his role must be in [Owner, Editor, Viewer], otherwise return
    if (memberRole !== OWNER && memberRole !== EDITOR && memberRole !== VIEWER) {
        const detailedMsg = 'The user with id: ' + memberId + ' doesn\'t have valid role: ' + memberRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    //========================= validation of the user who is performing the removal =========================//
    //get his uid
    const myUid = context.auth?.uid;

    //get his role and validate his membership in the document
    let myRole: any;
    try {
        myRole = (await db.ref('documents').child(docId).child('users')
            .child(myUid).child('role').get()).val();
    }
    catch (err) {
        const detailedMsg = 'The user who is performing the removal of a member is not himself a member ' +
            +'in the doc! or failed to get his role from (documents/' + docId + '/users/' + myUid + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found',
            'You are not a member in this document anymore!', detailedMsg);
    }

    //validation: his role must be in [Owner, Editor, Viewer], otherwise return
    if (myRole !== OWNER && myRole !== EDITOR && myRole !== VIEWER) {
        const detailedMsg = 'The user with id: ' + myUid + ' doesn\'t have valid role: ' + myRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    //=========================== if the user is removing himself (leaving the doc) ===========================//
    if (memberId === myUid && (myRole === EDITOR || myRole === VIEWER
        || (myRole === OWNER && (await getNumOfOwners(docId)) > 1)))
        return (await remMemFromDB(myUid, docId, memberRole));
    //Note: this condition is not allowing the owner to leave the doc if he is the only owner!

    //=================================== if a viewer is removing a member ===================================//
    else if (memberId !== myUid && myRole === VIEWER) {
        const detailedMsg = 'The user with uid: ' + myUid + ', is not allowed to remove member ' +
            'with id: ' + memberId + ' from doc with id: ' + docId + ' because he is a viewer!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition', 'You are a viewer. ' +
            'Viewers aren\'t allowed to remove other members in the document!', detailedMsg);
    }

    //================================ if an owner is removing another member  ================================//
    //========================= or if the editor is removing another editor or viewer =========================//
    else if (memberId !== myUid && (myRole === OWNER || (myRole === EDITOR && memberRole !== OWNER)))
        return (await remMemFromDB(memberId, docId, memberRole));

    //=================================== if an editor is removing an owner ===================================//
    else if (memberId !== myUid && (myRole === EDITOR && memberRole === OWNER)) {
        const detailedMsg = 'The user (role: Editor) with id: ' + myUid + ', is not allowed to remove an owner with' +
            ' id: ' + memberId + ' from doc with id: ' + docId + ' because only owners can remove other owners!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition',
            'Only owners are allowed to remove owners, but your role is: ' + myRole, detailedMsg);
    }

    //================= if the owner is removing himself and he is the only owner in the doc  =================//
    else {
        const detailedMsg = 'The owner of document id: ' + docId + ', with uid: ' + myUid +
            ', is not allowed to leave the document because he is the only owner in this doc!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition',
            'You are the only owner in this document, so you can\'t leave it.', detailedMsg);
    }
});

//========== Function: to remove the user from the document node and the document from the user node ==========//
async function remMemFromDB(memberId: string, docId: string, memberRole: string) {
    //remove the user from (documents/docId/users/userId)
    try {
        await db.ref('documents').child(docId).child('users').child(memberId).remove();
    } catch (err) {
        const detailedMsg = 'Failed to remove (documents/' + docId + '/users/' + memberId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }
    //remove this doc from (users/userId/sharedDocs/docId)
    return db.ref('users').child(memberId).child('sharedDocs').child(docId).remove().then(() => {
        //DONE!
        const detailedMsg = 'DONE: Successfully removed user with uid ' + memberId +
            ', with role: ' + memberRole + ', from document with id: ' + docId;
        console.log(detailedMsg);
        return {
            userRes: 'The user was successfully removed from the document!',
            detailedRes: detailedMsg
        };
    }).catch((err) => {
        const detailedMsg = 'Failed to remove (users/' + memberId + '/sharedDocs/' + docId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    });
}

//=========================== Function: check how many owners exist in the document ===========================//
async function getNumOfOwners(docId: string) {
    //get users from path (documents/docId/users)
    let membersSnapshots: any;
    try {
        membersSnapshots = (await db.ref('documents').child(docId).child('users').get()).val();
    } catch (err) {
        const detailedMsg = 'Failed to get (documents/' + docId + '/users) parent';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //validation: double-check the result, if undefined or null, return
    if (membersSnapshots === undefined || membersSnapshots === null) {
        const detailedMsg = 'Failed to get (documents/' + docId + '/users) parent';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //get all members ids
    const membersIds = Object.keys(membersSnapshots);
    //loop through all doc members and check how many owners is there
    let ownerNum = 0;
    for (let tempMemberId of membersIds) {
        const tempMemberRole = membersSnapshots[tempMemberId].role;
        if (tempMemberRole === OWNER) ownerNum++;
    }

    //print owners number of the doc and check for an internal error when owners are < 1
    console.log('There is ' + ownerNum + ' owners in doc with id: ' + docId);
    if (ownerNum < 1) {
        const detailedMsg = 'Error: owners of docId: ' + docId + ' are less than 1!!!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }
    else return ownerNum;
}