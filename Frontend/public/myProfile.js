//========= variables =========//
let uploadedFile = null;
let usersRef;
//====== invoke on load ======//
(function () {
    var firebaseConfig = {
        apiKey: "AIzaSyCaaBXjrgPKRry7SU2CqD9MQ-PAuWnvoi4",
        authDomain: "docbook-2021.firebaseapp.com",
        databaseURL: "https://docbook-2021-default-rtdb.firebaseio.com",
        projectId: "docbook-2021",
        storageBucket: "docbook-2021.appspot.com",
        messagingSenderId: "438833982715",
        appId: "1:438833982715:web:0bc7de4c296d4bea5cd45e",
        measurementId: "G-S5NMEZDF65"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    usersRef = firebase.database().ref("users");

    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        //get my user info
        firebase.database().ref("users").child(user.uid)
            .get().then(mySnapshot => {
                const userInfo = mySnapshot.val().userInfo;
                if (userInfo === null || userInfo === undefined) return;
                //name and email
                const name = userInfo.name;
                const email = userInfo.email;
                document.getElementById("name-field").value = name;
                document.getElementById("email-field").value = email;
                //profile pic
                const profilePic = userInfo.profilePic;
                //validation
                if (profilePic === undefined || profilePic === null || profilePic === "") return;
                //manipulate
                $("#profilePic-field").attr("src", "");
                $("#profilePic-field").attr("src", profilePic);
                document.getElementById("profilePic-field").style.height = '100%';
                document.getElementById("profilePic-field").style.width = '100%';
            });

        //listen to file chooser
        const uploadImgInput = document.getElementById("upload-img-input");
        uploadImgInput.addEventListener("change", handleFiles, false);
    });
})();

//============================ perform user logout ============================//
function logout() {
    firebase.auth().signOut().then(() => {
        console.log("User logged out!");
        $('#logoutModal').modal('hide');
        window.location.replace("index.html");
    }).catch((error) => {
        console.log(error);
    });
}

//================ save user's information (save changes btn) ================//
function userInfoSave() {
    firebase.auth().onAuthStateChanged(function (user) {
        //if current user = null then return
        if (!user) return;

        //get current email and name
        const currentEmail = user.email;
        const currentName = user.displayName;

        //get name and email fields content
        const newName = document.getElementById("name-field").value.trim();
        const newEmail = document.getElementById("email-field").value.trim();

        //validation if something is empty, return
        if (newName === null || newName === "" || newEmail === null || newEmail === "") {
            document.getElementById("info-modal-text").innerText = "Please fill all fields!";
            $('#infoModal').modal('show');
            return;
        }

        //validation if nothing changed, return 
        if ((uploadedFile === undefined || uploadedFile === null)
            && newName == currentName && newEmail == currentEmail) {
            document.getElementById("info-modal-text").innerText = "Nothing to change!";
            $('#infoModal').modal('show');
            return;
        }

        //if a new image was chosen from file chooser
        if (uploadedFile !== undefined && uploadedFile !== null)
            //upload image + update user info (with profileUrl)
            updateInfoWithPic(user, newName, currentEmail, newEmail);

        //if no image was chosen (update just the name and the email)
        else usersRef.child(user.uid).child("userInfo").update({
            name: newName,
            email: newEmail
        }).then(() => {
            //if the email wasn't changed
            if (currentEmail === newEmail) {
                document.getElementById("info-modal-text").innerText
                    = "Your account info were changed successfully!";
                $('#infoModal').modal('show');
            }

            //if the email was changed
            else {
                document.getElementById("info-modal-text").innerText
                    = "Changes successfully saved! Because the email was changed, " +
                    "for security reasons, you were logged out, please sign in again";
                $('#infoModal').modal('show');
                document.getElementById("info-ok-btn").onclick = function () {
                    window.location.replace("index.html");
                };
            }
        }).catch(err => {
            document.getElementById("info-modal-text").innerText
                = "Failed to change user info, " + err.message;
            $('#infoModal').modal('show');
        });
    });
}

//=============== to change the password (change password btn) ===============//
function changePassword() {
    firebase.auth().onAuthStateChanged(function (user) {
        //if current user = null then return
        if (!user) return;

        //get fields
        const oldPass = document.getElementById("oldPass-field").value;
        const newPass = document.getElementById("newPass-field").value;
        const confNewPass = document.getElementById("confNewPass-field").value;

        //validation
        if (oldPass === undefined || oldPass === null || oldPass === ""
            || newPass === undefined || newPass === null || newPass === ""
            || newPass !== confNewPass) {
            document.getElementById("info-modal-text").innerText = "Please fill all fields " +
                "and insure that password conformation matches the new password";
            $('#infoModal').modal('show');
            return;
        }

        //reauthentication
        const credential = firebase.auth.EmailAuthProvider.credential(
            user.email,
            oldPass
        );
        user.reauthenticateWithCredential(credential).then(() => {
            //now update the password
            user.updatePassword(newPass).then(() => {
                document.getElementById("info-modal-text").innerText
                    = "Successfully changed password!";
                $('#infoModal').modal('show');
            }).catch((error) => {
                document.getElementById("info-modal-text").innerText
                    = "Failed to change user password, " + error.message;
                $('#infoModal').modal('show');
            });
        }).catch((error) => {
            console.log("Failed to reauthenticate user, " + error.message);
            document.getElementById("info-modal-text").innerText
                = "The old password is not right!";
            $('#infoModal').modal('show');
        });
    });
}

//============== to remove user's account (remove account btn)==============//
function removeAccount() {
    firebase.auth().onAuthStateChanged(function (user) {
        //if current user = null then return
        if (!user) return;

        //get pass field
        const confPass = document.getElementById("confPass-field").value;

        //validation
        if (confPass === undefined || confPass === null || confPass === "") {
            document.getElementById("info-modal-text").innerText
                = "Please enter your password!";
            $('#infoModal').modal('show');
            return;
        }

        //reauthentication
        const credential = firebase.auth.EmailAuthProvider.credential(
            user.email,
            confPass
        );
        user.reauthenticateWithCredential(credential).then(() => {
            //now remove the user 
            user.delete().then(() => {
                document.getElementById("info-modal-text").innerText
                    = "Your account was successfully removed!";
                $('#infoModal').modal('show');
                document.getElementById("info-ok-btn").onclick = function () {
                    window.location.replace("index.html");
                };
            }).catch((error) => {
                document.getElementById("info-modal-text").innerText
                    = "Failed to remove the user, " + error.message;
                $('#infoModal').modal('show');
            });
        }).catch((error) => {
            console.log("Failed to reauthenticate user, " + error.message);
            document.getElementById("info-modal-text").innerText
                = "The password is not right!";
            $('#infoModal').modal('show');
        });
    });
}

//upload profile image to cloud storage and get the url and store it in db
function updateInfoWithPic(user, newName, currentEmail, newEmail) {
    // Create a root reference
    const usersStorageRef = firebase.storage().ref("users");
    // Create a reference to 'mountains.jpg'
    const fileExtension = uploadedFile.name.split('.').pop();
    const profilePicRef = usersStorageRef.child(user.uid + '/profilePic.' + fileExtension);
    var metadata = { contentType: 'image/' + fileExtension };
    profilePicRef.put(uploadedFile, metadata).then((snapshot) => {
        console.log('Uploaded a blob or file!');
        profilePicRef.getDownloadURL().then((downloadURL) => {
            //update user info and profile pic
            usersRef.child(user.uid).child("userInfo").update({
                name: newName,
                email: newEmail,
                profilePic: downloadURL
            }).then(() => {
                //if the email wasn't changed
                if (currentEmail === newEmail) {
                    document.getElementById("info-modal-text").innerText
                        = "Your account info were changed successfully!";
                    $('#infoModal').modal('show');
                }

                //if the email was changed
                else {
                    document.getElementById("info-modal-text").innerText
                        = "Changes successfully saved! Because the email was changed, " +
                        "for security reasons, you were logged out, please sign in again";
                    $('#infoModal').modal('show');
                    document.getElementById("info-ok-btn").onclick = function () {
                        window.location.replace("index.html");
                    };
                }
            }).catch(err => {
                document.getElementById("info-modal-text").innerText
                    = "Failed to change user info, " + err.message;
                $('#infoModal').modal('show');
            });

        });
    });
}

//when you click on the image, it perform a click on the hidden file input element 
function onImgClick() { $('#upload-img-input').trigger('click'); }

//when the user clicks open image on the file chooser, display the image in the circle
function handleFiles() {
    const fileList = this.files;
    if (FileReader && fileList && fileList.length) {
        uploadedFile = fileList[0];
        const fr = new FileReader();
        fr.onload = function () {
            //to set the image
            $("#profilePic-field").attr("src", fr.result);
            document.getElementById("profilePic-field").style.height = '100%';
            document.getElementById("profilePic-field").style.width = '100%';
        }
        fr.readAsDataURL(uploadedFile);
    }
}