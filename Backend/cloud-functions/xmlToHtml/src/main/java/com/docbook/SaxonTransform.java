package com.docbook;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.cloud.functions.Context;
import com.google.cloud.functions.RawBackgroundFunction;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;
import javax.xml.transform.stream.StreamSource;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Xslt30Transformer;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;

public class SaxonTransform implements RawBackgroundFunction {

  private static final Gson gson = new Gson();
  private static final Logger logger = Logger.getLogger(SaxonTransform.class.getName());
  // firebase
  private static FirebaseDatabase database;
  private static DatabaseReference ref;
  // saxon transform
  private static XsltExecutable stylesheet;
  private static Processor processor;

  public SaxonTransform() {
    // firebase init
    FirebaseApp.initializeApp();
    database = FirebaseDatabase.getInstance("https://docbook-2021-default-rtdb.firebaseio.com");
    ref = database.getReference("documents");

    // saxon XSL compile init
    try {
      processor = new Processor(false);
      XsltCompiler compiler = processor.newXsltCompiler();
      stylesheet = compiler.compile(new StreamSource(new File("saxon_docbook/xslt/docbook.xsl")));
    } catch (SaxonApiException e) {
      logger.info("XSL compile error: " + e.getMessage());
      e.printStackTrace();
    }
  }

  @Override
  public void accept(String json, Context context) throws InterruptedException {
    // get the xml child
    JsonObject body = gson.fromJson(json, JsonObject.class);
    // if xmlData child was removed, return
    if (!body.has("delta") || body.get("delta") == null) {
      logger.info("dataXML was removed, so no need to transform it to html!");
      return;
    }
    String xmlData = body.get("delta").getAsString();

    // get the path to the htmlData child
    String[] pathArr = context.resource().split("/");
    // get the docId from the path
    String docId = pathArr[pathArr.length - 3];

    try {
      // transform xml to html
      String htmlData = transformXmlToHtml(xmlData);

      // save htmlData to firebase
      ApiFuture<Void> future = ref.child(docId + "/metadata/htmlData").setValueAsync(htmlData);

      // listen when the save to firebase is finished!
      ApiFutures.addCallback(future, new ApiFutureCallback<Void>() {
        @Override
        public void onFailure(Throwable t) {
          logger.info("Failed to save the transformed htmlData to firebase: " + t.getMessage());
        }

        @Override
        public void onSuccess(Void v) {
          logger.info("Html transformed data was saved to firebase successfully!");
        }
      }, MoreExecutors.directExecutor());

      // VERY IMPORTANT: wait for the realtime database save!
      try {
        future.get();
      } catch (ExecutionException e) {
        logger.info("Failed to wait to save htmlData to firebase: " + e.getMessage());
        e.printStackTrace();
      }
    } catch (SaxonApiException e1) {
      logger.info("Failed to transform xml to html using Saxon: " + e1.getMessage());
      e1.printStackTrace();
    }
  }

  private String transformXmlToHtml(String xmlStr) throws SaxonApiException {
    // transformer init
    Xslt30Transformer transformer = stylesheet.load30();

    // prepare the xml input
    StringReader strReader = new StringReader(xmlStr);
    StreamSource xmlInput = new StreamSource(strReader);

    // prepare the output html
    StringWriter strWriter = new StringWriter();
    Serializer htmlOutput = processor.newSerializer(strWriter);

    // set properties
    htmlOutput.setOutputProperty(Serializer.Property.METHOD, "html");
    htmlOutput.setOutputProperty(Serializer.Property.INDENT, "yes");

    // transform
    transformer.transform(xmlInput, htmlOutput);

    // get html output as string
    String strHTML = strWriter.toString();
    return strHTML;
  }
}
