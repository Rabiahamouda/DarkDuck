import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();
const GENERAL_ERR_MSG = 'Something went wrong!';
//======== roles ========//
const OWNER = 'Owner';
const EDITOR = 'Editor';
const VIEWER = 'Viewer';

export const changeMemberRole = functions.https.onCall(async (data, context) => {
    //For changing the role of a member in a document, you need these arguments:
    /*arguments:
    - docId: string
    - memberEmail: string
    - memberNewRole: string
    */
    const docId = data.docId;
    const memberEmail = data.memberEmail;
    const memberNewRole = data.memberNewRole;

    //================================== auth and argument validations ==================================//
    //arguments validation
    if (docId === null || memberEmail === null || docId === undefined || memberEmail === undefined
        || (memberNewRole !== EDITOR && memberNewRole !== OWNER && memberNewRole !== VIEWER)) {
        const detailedMsg = 'One or more invalid arguments!'
        console.log(detailedMsg)
        throw new functions.https.HttpsError('invalid-argument', GENERAL_ERR_MSG, detailedMsg);
    }

    //auth validation
    if (context.auth === undefined || context.auth === null) {
        const detailedMsg = 'The function must be called only if the invoker is authenticated!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition', GENERAL_ERR_MSG, detailedMsg);
    }

    //=============================== validation of the user to be changed ===============================//
    //get his uid and check if he exist in the system
    let memberId: any;
    try {
        memberId = (await admin.auth().getUserByEmail(memberEmail)).uid;
    } catch (err) {
        const detailedMsg = 'User with email: ' + memberEmail + ', doesn\'t exist!';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('failed-precondition', detailedMsg, detailedMsg);
    }

    //get his role and validate his membership in the document
    let memberOldRole: any;
    try {
        memberOldRole = (await db.ref('documents').child(docId).child('users')
            .child(memberId).child('role').get()).val();
    } catch (err) {
        const detailedMsg = 'The user whose role is about to be changed is not a member in the doc, ' +
            +'or failed to get his role from (documents/' + docId + '/users/' + memberId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('failed-precondition', 'You are trying to change a role of a user ' +
            'who is not a member in this document anymore!', detailedMsg);
    }

    //validation: his role must be in [Owner, Editor, Viewer], otherwise return
    if (memberOldRole !== OWNER && memberOldRole !== EDITOR && memberOldRole !== VIEWER) {
        const detailedMsg = 'The user with id: ' + memberId + ' doesn\'t have valid role: ' + memberOldRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    //validation: if the new assigned role is the same as the current role of the user, then return
    if (memberOldRole === memberNewRole) {
        const detailedMsg = 'The new role assigned to the user with uid: ' + memberId +
            ', is the same as his current role: ' + memberOldRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('failed-precondition',
            'The new role assigned to the member is the same as his current role!', detailedMsg);
    }

    //======================= validation of the user who is performing the role change =======================//
    //get his uid
    const myUid = context.auth?.uid;

    //get his role and validate his membership in the document
    let myRole: any;
    try {
        myRole = (await db.ref('documents').child(docId).child('users')
            .child(myUid).child('role').get()).val();
    }
    catch (err) {
        const detailedMsg = 'The user who is performing the role change of a member is not himself a member ' +
            +'in the doc! or failed to get his role from (documents/' + docId + '/users/' + myUid + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found',
            'You are not a member anymore in this document to change the role of a member!', detailedMsg);
    }

    //validation: his role must be in [Owner, Editor, Viewer], otherwise return
    if (myRole !== OWNER && myRole !== EDITOR && myRole !== VIEWER) {
        const detailedMsg = 'The user with id: ' + myUid + ' doesn\'t have valid role: ' + myRole;
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    //=================================== if the user is changing his role ===================================//
    if (memberId === myUid) {
        switch (myRole) {
            case OWNER:
                if ((await getNumOfOwners(docId)) > 1)
                    return (await changeRoleOfMember(myUid, docId, OWNER, memberNewRole));
                else {
                    const detailedMsg = 'Member with uid: ' + myUid +
                        ', is an owner who is trying to change his role to: ' + memberNewRole +
                        ' but he is the only owner in this document, so his request is denied! DocId: ' + docId;
                    console.log(detailedMsg);
                    throw new functions.https.HttpsError('failed-precondition', 'You are the only owner in this document, ' +
                        'so you are not allowed to change your role!', detailedMsg);
                }

            case EDITOR:
                if (memberNewRole === VIEWER)
                    return (await changeRoleOfMember(myUid, docId, EDITOR, VIEWER));
                else {
                    const detailedMsg = 'Member with uid: ' + myUid +
                        ', is an editor who is trying to change his role to: ' + memberNewRole +
                        '. Editors are only allowed to change their role to a viewer! DocId: ' + docId;
                    console.log(detailedMsg);
                    throw new functions.https.HttpsError('failed-precondition', 'You are an editor, so you are only ' +
                        'allowed to change your role to a viewer!', detailedMsg);
                }
            case VIEWER:
                const detailedMsg = 'Member with uid: ' + myUid +
                    ', is a viewer who is trying to change his role to: ' + memberNewRole +
                    '. Viewers are not allowed to change their role! DocId: ' + docId;
                console.log(detailedMsg);
                throw new functions.https.HttpsError('failed-precondition', 'You are not allowed to ' +
                    'change the role of document\'s members because you are a viewer!', detailedMsg);
            default: return null;
        }
    }

    //============================ if the user is changing the role of another user ============================//
    else switch (myRole) {
        case OWNER:
            return (await changeRoleOfMember(memberId, docId, memberOldRole, memberNewRole));
        case EDITOR:
            if (memberOldRole !== OWNER && memberNewRole !== OWNER)
                return (await changeRoleOfMember(memberId, docId, memberOldRole, memberNewRole));
            else {
                const detailedMsg = 'Member with uid: ' + myUid + ', is an editor who is ' +
                    'trying to change the role of a member with uid: ' + memberId + ', from: ' +
                    memberOldRole + ' to: ' + memberNewRole + '. Editors are only allowed to change ' +
                    'the role of a viewer to an editor, or the role of an editor to a viewer! DocId:' + docId;
                console.log(detailedMsg);
                throw new functions.https.HttpsError('failed-precondition', 'You are an editor, ' +
                    'so you are only allowed to change the role of a viewer to an editor, ' +
                    'or the role of an editor to a viewer!', detailedMsg);
            }
        case VIEWER:
            const detailedMsg = 'Member with uid: ' + myUid + ', is a viewer who is ' +
                'trying to change the role of a member with uid: ' + memberId + ', from: ' +
                memberOldRole + ' to: ' + memberNewRole + '. Viewers are not allowed to change ' +
                'the role of any member in this document! DocId:' + docId;
            console.log(detailedMsg);
            throw new functions.https.HttpsError('failed-precondition', 'You are a viewer, ' +
                'so you are not allowed to change the role of any member in this document', detailedMsg);
        default: return null;
    }
});

//=================================== Function: change the role of a member ===================================//
async function changeRoleOfMember(memberId: string, docId: string, oldRole: string, newRole: string) {
    try {
        await db.ref('users').child(memberId).child('sharedDocs').child(docId).update({ role: newRole });
    } catch (err) {
        const detailedMsg = 'Failed to change the role of user with uid: ' + memberId + ', from: ' +
            oldRole + ' to: ' + newRole + ', in (users/' + memberId + '/sharedDocs/' + docId + ')';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }

    return db.ref('documents').child(docId).child('users').child(memberId)
        .update({ role: newRole }).then(() => {
            //DONE!
            const detailedMsg = 'DONE: Successfully changed the role of  user with uid ' + memberId +
                ', from role: ' + oldRole + ', to role: ' + newRole + ', in document with id: ' + docId;
            console.log(detailedMsg);
            return {
                userRes: 'The role of the user was successfully changed to: ' + newRole + '!',
                detailedRes: detailedMsg
            };
        }).catch(err => {
            const detailedMsg = 'Failed to change the role of user with uid: ' + memberId + ', from: ' +
                oldRole + ' to: ' + newRole + ', in (documents/' + docId + '/users/' + memberId + ')';
            console.log(detailedMsg, err);
            throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
        })
}

//=========================== Function: check how many owners exist in the document ===========================//
async function getNumOfOwners(docId: string) {
    //get users from path (documents/docId/users)
    let membersSnapshots: any;
    try {
        membersSnapshots = (await db.ref('documents').child(docId).child('users').get()).val();
    } catch (err) {
        const detailedMsg = 'Failed to get (documents/' + docId + '/users) parent';
        console.log(detailedMsg, err);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //validation: double-check the result, if undefined or null, return
    if (membersSnapshots === undefined || membersSnapshots === null) {
        const detailedMsg = 'Failed to get (documents/' + docId + '/users) parent';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('not-found', GENERAL_ERR_MSG, detailedMsg);
    }

    //get all members ids
    const membersIds = Object.keys(membersSnapshots);
    //loop through all doc members and check how many owners is there
    let ownerNum = 0;
    for (let tempMemberId of membersIds) {
        const tempMemberRole = membersSnapshots[tempMemberId].role;
        if (tempMemberRole === OWNER) ownerNum++;
    }

    //print owners number of the doc and check for an internal error when owners are < 1
    console.log('There is ' + ownerNum + ' owners in doc with id: ' + docId);
    if (ownerNum < 1) {
        const detailedMsg = 'Error: owners of docId: ' + docId + ' are less than 1!!!';
        console.log(detailedMsg);
        throw new functions.https.HttpsError('internal', GENERAL_ERR_MSG, detailedMsg);
    }
    else return ownerNum;
}