import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();

export const onUpdateUserInfo = functions.database.ref('/users/{userId}/userInfo')
    .onUpdate(async (snapshot, context) => {
        const myUid = context.params.userId;
        //new user info
        const newName = snapshot.after.child('name').val();
        const newEmail = snapshot.after.child('email').val();
        const newProfilePic = snapshot.after.child('profilePic').val();

        //============================ update user info in auth section ============================//
        try {
            await admin.auth().updateUser(myUid, {
                displayName: newName,
                email: newEmail,
                photoURL: newProfilePic
            });
        } catch (err) {
            console.log('Failed to update user\'s info in auth section! userId: ' + myUid, err);
            return null;
        }

        //====================== update user info in (documents/users) parent ======================//
        //get my sharedDocs parent with all document ids that I'm contributed in
        let sharedDocs: any;
        try {
            sharedDocs = (await db.ref('users').child(myUid).child('sharedDocs').get()).val();
        }
        catch (err) {
            console.log('Failed to read user with uid: ' + myUid +
                ' while trying to read his sharedDocs child', err);
            return null;
        }

        //if no sharedDocs, return
        if (sharedDocs === undefined || sharedDocs === null) {
            console.log('User with uid: ' + myUid + ' doesn\'t contribute in any document ' +
                '(no sharedDocs child), so nothing needs to be updated!');
            return null;
        }

        //get all sharedDocs ids
        const sharedDocsIds = Object.keys(sharedDocs);
        //for saving update promises
        const updatePromises = [];

        //loop through all docs the user is contributed in and update his user info accordingly
        for (let docId of sharedDocsIds) {
            updatePromises.push(db.ref('documents').child(docId).child('users')
                .child(myUid).update({
                    name: newName,
                    email: newEmail,
                    profilePic: newProfilePic
                }).catch(err => {
                    console.log('Failed to update user\'s info in (documents/' + docId
                        + '/users/' + myUid + ') child', err);
                }));
        }

        //return when all (documents/docId/users/userId) nodes are updated with the new user info
        return Promise.all(updatePromises).then(() => {
            //DONE!: updated all user's nodes in (documents/docId/users/userId) with new user's data!
            console.log('DONE: Successfully updated user\'s data in all documents he contributed in!' +
                ' In all (documents/<docId>/users/' + myUid + ') paths where docId is dynamic');
        }).catch(err => {
            console.log('Failed to update one or more user\'s info in (documents/<docId>/users/'
                + myUid + ')!', err);
        });
    });