import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();

export const onRemoveDoc = functions.database.ref('/documents/{docId}')
    .onDelete((snapshot, context) => {
        const docId = context.params.docId;

        //get all members ids (document/users) node
        const membersIds = Object.keys(snapshot.child('users').val());
        //for saving remove promises
        const removePromises = [];

        //loop through members of the document and remove this doc from their sharedDocs child
        for (let memberId of membersIds) {
            removePromises.push(db.ref('users').child(memberId).child('sharedDocs')
                .child(docId).remove().catch(err => {
                    console.log('Failed to remove (users/'
                        + memberId + '/sharedDocs/' + docId + ')', err);
                }));
        }

        //return when all documents are removed from all sharedDocs nodes of all doc members
        return Promise.all(removePromises).then(() => {
            //remove document bucket from cloud storage
            return admin.storage().bucket().deleteFiles({ prefix: 'documents/' + docId }).then(() => {
                //DONE!: removed all documents in sharedDocs node for all members of the doc
                console.log('DONE: Successfully removed all documents present in all members\' ' +
                    'sharedDocs nodes, and from cloud storage, so the document is completely' +
                    'removed from DB! DocId: ' + docId);
            }).catch(err => {
                console.log('Failed to remove document storage bucket (documents/' + docId + ')', err);
            });
        }).catch(err => {
            console.log('Failed to remove one or more document from members\'' +
                ' sharedDocs nodes! So some parts of the document still exist in the DB.' +
                ' DocId: ' + docId, err);
        });
    });