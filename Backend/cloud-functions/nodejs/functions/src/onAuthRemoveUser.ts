import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.database();
const OWNER = 'Owner';

export const onAuthRemoveUser = functions.auth.user().onDelete(async (user) => {
    const myUid = user.uid;
    //get user's sharedDocs
    let sharedDocs: any;
    try {
        sharedDocs = (await db.ref('users').child(myUid).child('sharedDocs').get()).val();
    }
    catch (err) {
        console.log('Failed to read sharedDocs child of a user with uid: ' + myUid, err);
        return null;
    }

    //if no sharedDocs, then you can remove user node from users root parent immediately (users/userId)
    if (sharedDocs === undefined || sharedDocs === null) {
        return db.ref('users').child(myUid).remove().then(() => {
            //remove user bucket from cloud storage
            return admin.storage().bucket().deleteFiles({ prefix: 'users/' + myUid }).then(() => {
                //DONE!
                console.log('DONE: The user with id: ' + myUid + ', was successfully removed! ' +
                    'Note: he doesn\'t have sharedDocs child, so just his user child (users/uid) ' +
                    'and storage (if any) bucket were removed');
            }).catch(err => {
                console.log('Failed to remove user storage bucket (users/' + myUid + ')', err);
            });
        }).catch(err => {
            console.log('Failed to remove user doc from users root parent, uid: ' + myUid, err);
        });
    }

    //remove user node from users root parent (users/userId)
    try {
        await db.ref('users').child(myUid).remove();
    } catch (err) {
        console.log('Failed to remove user doc from users root parent, uid: ' + myUid, err);
        return null;
    }

    //get all sharedDocs ids
    const sharedDocsIds = Object.keys(sharedDocs);
    //for saving remove promises (remove member or document)
    const removePromises: any = [];
    //read users parent promises
    const readUsersPromise: any = [];

    //loop through docIds and remove all users' nodes from (documents/users)
    for (let docId of sharedDocsIds) {

        //read all users parent nodes (members) of shared docs
        readUsersPromise.push(db.ref('documents').child(docId).child('users')
            .get().then((membersSnapshots) => {
                //invoke function to check if I'm the only owner of the doc
                const removeDoc = amITheOnlyOwner(docId, membersSnapshots.val(), myUid);
                if (removeDoc === null) return;

                //if I'm the only owner of the doc, then remove the doc!
                if (removeDoc)
                    removePromises.push(db.ref('documents').child(docId).remove().catch(err => {
                        console.log('Failed to remove document with id: ' + docId, err);
                    }));

                //if I'm not owner or not the only owner of the doc, then just remove me
                else removePromises.push(db.ref('documents').child(docId)
                    .child('users').child(myUid).remove().catch(err => {
                        console.log('Failed to remove user\'s node (member) from (documents/'
                            + docId + '/users/' + myUid + ')', err);
                    }));

            }).catch(err => {
                console.log('Failed to read users parent in docId: ' + docId, err);
            }));
    }

    //return when all users' nodes are removed
    return Promise.all(removePromises).then(() => {
        return Promise.all(readUsersPromise).then(() => {
            //remove user bucket from cloud storage
            return admin.storage().bucket().deleteFiles({ prefix: 'users/' + myUid }).then(() => {
                //DONE!:
                console.log('DONE: Successfully removed everything related to user with uid: ' + myUid
                    + ' from database (users/uid & documents/users/uid) and cloud storage (users/uid) if any');
            }).catch(err => {
                console.log('Failed to remove user storage bucket (users/' + myUid + ')', err);
            });
        }).catch(err => {
            console.log('Failed to read users parent node for one or more document! uid: ' + myUid, err);
        });
    }).catch(err => {
        console.log('Failed to remove one or more user\'s nodes in (documents/users)! uid: ' + myUid, err);
    });
});

//This function returns true when myUid user is the only owner of doc with id: docId
function amITheOnlyOwner(docId: string, membersSnapshots: any, myUid: string) {
    let amIOwner = false;
    //validation: double-check the result, if undefined or null, return
    if (membersSnapshots === undefined || membersSnapshots === null) {
        const detailedMsg = 'Failed to get (documents/' + docId + '/users) parent';
        console.log(detailedMsg);
        return null;
    }

    //get all members ids
    const membersIds = Object.keys(membersSnapshots);
    //loop through all doc members and check how many owners is there
    let ownerNum = 0;
    for (let tempMemberId of membersIds) {
        const tempMemberRole = membersSnapshots[tempMemberId].role;
        if (tempMemberRole === OWNER) {
            ownerNum++;
            if (myUid === tempMemberId) amIOwner = true;
        }
    }

    //print owners number of the doc and check for an internal error when owners are < 1
    console.log('There is ' + ownerNum + ' owners in doc with id: ' + docId);
    if (ownerNum < 1) {
        const detailedMsg = 'Error: owners of docId: ' + docId + ' are less than 1!!!';
        console.log(detailedMsg);
        return null;
    }

    //return true only if I'm owner and no one else is owner in this document
    return (amIOwner && ownerNum === 1)
}