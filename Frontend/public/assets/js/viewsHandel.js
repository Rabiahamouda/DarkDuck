$(document).ready(function () {
    //on html hide click
    $("#htmlHide").click(function () {
        console.log("hide html");
        $(".split.left").css("display", "none");
        $(".left1 h5").css("display", "none");
        $(".split.right").css("width", "100%");
        $(".left1").css("display", "none");
        $(".right1").css("width", "53%");
        $("#docHide").css("display", "none");
        $("#showBothLeft").css("display", "block");
        $("div.vl").css("display", "none");
        $(".feature-icon .mid").css("margin-left", "45px");

    });

    //on show both click (after hiding the html)
    $("#showBothLeft").click(function () {
        console.log("show both left");
        $(".split.right").css("display", "block");
        $(".split.left").css("display", "block");
        $(".right1 h5").css("display", "block");
        $(".left1 h5").css("display", "block");
        $(".split.left").css("width", "50%");
        $(".split.right").css("width", "50%");
        $(".right1").css("display", "flex");
        $(".left1").css("display", "flex");
        $(".right1").css("width", "29%");
        $(".left1").css("width", "29%");
        $("#docHide").css("display", "block");
        $("#showBothLeft").css("display", "none");
        $("div.vl").css("display", "inline-block");
        $(".feature-icon .mid").css("margin-left", "0");
    });

    //on show both click (after hiding the xml)
    $("#showBothRight").click(function () {
        console.log("show both right");
        $(".split.right").css("display", "block");
        $(".split.left").css("display", "block");
        $(".right1 h5").css("display", "block");
        $(".left1 h5").css("display", "block");
        $(".split.left").css("width", "50%");
        $(".split.right").css("width", "50%");
        $(".right1").css("display", "flex");
        $(".left1").css("display", "flex");
        $(".right1").css("width", "29%");
        $(".left1").css("width", "29%");
        $("#htmlHide").css("display", "block");
        $("#showBothRight").css("display", "none");
        $("ul.feature-icon").css("flex-direction", "row");
        $(".left1").css("flex-direction", "row");
        $("div.vl").css("display", "inline-block");
        $(".feature-icon .mid").css("margin-left", "0");
    });

    //on xml hide click
    $("#docHide").click(function () {
        console.log("hide xml");
        $(".split.right").css("display", "none");
        $(".right1 h5").css("display", "none");
        $(".split.left").css("width", "100%");
        $(".right1").css("display", "none");
        $(".left1").css("width", "53%");
        $("#htmlHide").css("display", "none");
        $("#showBothRight").css("display", "block");
        $("ul.feature-icon").css("flex-direction", "row-reverse");
        $(".left1").css("flex-direction", "row-reverse");
        $("div.vl").css("display", "none");
        $(".feature-icon .mid").css("margin-left", "45px");
    });
});

// Mob Preview Btn
$(document).ready(function (e) {
    let isSwitched = false;
    // This will fire each time the window is resized:
    $(window).resize(function () {
        // if larger or equal
        if ($(window).width() < 992) {            
            //on xml click (switch to xml view) 
            $("#viewhtml").click(function (e) {
                isSwitched = true;
                $(".split.right").css("display", "none");
                $(".split.left").css("display", "block");
                $(".xml-nav").css("display", "none");
                $(".html-nav").css("display", "flex");
                // $(".vl").css("display", "none");
                return true;
            });

            // on preview click (switch to html view) 
            $("#viewxml").click(function (e) {
                isSwitched = true;
                $(".split.right").css("display", "block");
                $(".split.left").css("display", "none");
                $(".xml-nav").css("display", "flex");
                $(".html-nav").css("display", "none");
                // $(".vl").css("display", "none");
                return true;
            });
            // if bigger or equals 992
        } else if (isSwitched) {
            isSwitched = false;
            $(".xml-nav").css("display", "flex");
            $(".html-nav").css("display", "none");
            $(".split.right").css("display", "block");
            $(".split.left").css("display", "block");
        }
    }).resize(); // This will simulate a resize to trigger the initial run.
});