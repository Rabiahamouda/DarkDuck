package com.docbook;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.Timestamp;
import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import java.util.UUID;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConvertFOP implements HttpFunction {

  private static final Gson gson = new Gson();
  private static final Logger logger = Logger.getLogger(ConvertFOP.class.getName());
  // Firebase
  private static FirebaseDatabase database;
  private static DatabaseReference docsRef;
  // The ID of your GCP project
  private static final String projectId = "docbook-2021";
  // The ID of your GCS bucket
  private static final String bucketName = "docbook-2021.appspot.com";
  // Some variables
  private String xmlData = null;
  private Long xmlUpdatedAt = null;
  private Long pdfUpdatedAt = null;
  private String pdfUrl = null;
  private boolean isCurrentUserMember = false;

  public ConvertFOP() {
    // firebase init
    try {
      FileInputStream serviceAccount = new FileInputStream("docbook-2021-firebase-adminsdk-wtuwp-4d19f2adce.json");
      FirebaseOptions options = FirebaseOptions.builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
          .setDatabaseUrl("https://docbook-2021-default-rtdb.firebaseio.com").build();
      FirebaseApp.initializeApp(options);
    } catch (FileNotFoundException e) {
      logger.severe("Internal error in the Firebase init: " + e.getMessage());
      e.printStackTrace();
      return;
    } catch (IOException e) {
      logger.severe("Internal error in the Firebase init: " + e.getMessage());
      e.printStackTrace();
      return;
    }
    // database ref
    database = FirebaseDatabase.getInstance("https://docbook-2021-default-rtdb.firebaseio.com");
    docsRef = database.getReference("documents");
  }

  @Override
  public void service(HttpRequest request, HttpResponse response) throws IOException {
    // ========================== Set CORS headers =========================//
    // Allows GETs from any origin with the Content-Type
    // header and caches preflight response for 3600s
    response.appendHeader("Access-Control-Allow-Origin", "*");
    if ("OPTIONS".equals(request.getMethod())) {
      response.appendHeader("Access-Control-Allow-Methods", "POST");
      response.appendHeader("Access-Control-Allow-Headers", "Content-Type");
      response.appendHeader("Access-Control-Max-Age", "3600");
      response.setStatusCode(HttpURLConnection.HTTP_NO_CONTENT);
      return;
    }
    BufferedWriter writer = response.getWriter(); // to send a response back

    // =============== Read docId and tokenId from request params ===============//
    String docId;
    String tokenId;
    // Parse JSON request and check for "docId" and "tokenId" fields
    try {
      JsonElement requestParsed = gson.fromJson(request.getReader(), JsonElement.class);
      JsonObject requestJson = null;

      if (requestParsed != null && requestParsed.isJsonObject()) {
        requestJson = requestParsed.getAsJsonObject();
      }

      // For docId argument
      if (requestJson != null && requestJson.has("docId")) {
        docId = requestJson.get("docId").getAsString();
      } else {
        response.setStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
        logger.severe("Invalid request docId argument!");
        return;
      }

      // For tokenId argument
      if (requestJson != null && requestJson.has("tokenId")) {
        tokenId = requestJson.get("tokenId").getAsString();
      } else {
        response.setStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
        logger.severe("Invalid request tokenId argument!");
        return;
      }

    } catch (JsonParseException e) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("Error parsing JSON: " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // create a Semaphore with 0 initial permits, for db read in the next 2 sections
    final Semaphore semaphore = new Semaphore(0);

    // ================= Check authenticity and doc membership ================= //
    String myUid;
    try {
      myUid = FirebaseAuth.getInstance().verifyIdToken(tokenId).getUid();
      // --- Checks if current user (with myUid) is a member in the document ---//
      docsRef.child(docId + "/users/" + myUid).addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
          if (dataSnapshot.getValue() != null)
            isCurrentUserMember = true;
          else
            logger.severe("Failed to generate PDF. User with uid: " + myUid + " is not a member in docId: " + docId);
          // increments 1 to semaphore
          semaphore.release();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
          response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
          logger.severe(
              "Failed to read (documents/" + docId + "/users/" + myUid + "). Error: " + databaseError.getDetails());
          // increments 1 to semaphore
          semaphore.release();
        }
      });
    }
    // ------ If the function called with a non authenticated user, return ------//
    catch (FirebaseAuthException e) {
      logger.severe("The function must be called only if the invoker is authenticated!");
      e.printStackTrace();
      return;
    }

    // -------------- waits for document member semaphore release --------------//
    try {
      // decrements 1 to semaphore, if successful, it returns, if not (semaphore=0),
      // then it waits until the semaphore is > 0 to be able to decrement it
      semaphore.acquire();
    } catch (InterruptedException e) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("Error in invoking semaphore acquire method for xmlData. " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // ---- if current user wasn't found as a member for some reason, return ----//
    if (!isCurrentUserMember) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("Failed to find user with uid: " + myUid + " in users parent in docId: " + docId);
      return;
    }

    // ====================== Get xml data from database ======================//
    docsRef.child(docId + "/metadata").addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        // ---------- get xmlData ----------//
        if (dataSnapshot.hasChild("xmlData") && dataSnapshot.child("xmlData").getValue() != null)
          xmlData = dataSnapshot.child("xmlData").getValue().toString();
        else
          logger.severe("xmlData child doesn't exist (null) for docId: " + docId);

        // ------- get xmlUpdatedAt -------//
        if (dataSnapshot.hasChild("xmlUpdatedAt") && dataSnapshot.child("xmlUpdatedAt").getValue() != null)
          xmlUpdatedAt = (long) dataSnapshot.child("xmlUpdatedAt").getValue();
        else
          logger.severe("xmlUpdatedAt child doesn't exist (null) for docId: " + docId);

        // ------- get pdfUpdatedAt -------//
        if (dataSnapshot.hasChild("pdfUpdatedAt") && dataSnapshot.child("pdfUpdatedAt").getValue() != null)
          pdfUpdatedAt = (long) dataSnapshot.child("pdfUpdatedAt").getValue();

        // ------- get pdfUrl -------//
        if (dataSnapshot.hasChild("pdfUrl") && dataSnapshot.child("pdfUrl").getValue() != null)
          pdfUrl = dataSnapshot.child("pdfUrl").getValue().toString();

        // -- increments 1 to semaphore --//
        semaphore.release();
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
        response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
        logger.severe("Failed to read (documents/" + docId + "/metadata). Error: " + databaseError.getDetails());
        // increments 1 to semaphore
        semaphore.release();
      }
    });

    // ------------- waits for xmlData semaphore release -------------//
    try {
      semaphore.acquire();
    } catch (InterruptedException e) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("Error in invoking semaphore acquire method for xmlData. " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // ----------- if xmlData/xmlUpdatedAt = null, return -----------//
    if (xmlData == null || xmlUpdatedAt == null) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("Failed to read xmlData or xmlUpdatedAt children for docId: " + docId);
      return;
    }

    // ----- if no changes have been done since the last trans. -----//
    if (pdfUpdatedAt != null && xmlUpdatedAt < pdfUpdatedAt) {
      logger.info("No need to transform docId: " + docId + " because no"
          + " changes have been done since the last transformation");
      writer.write(pdfUrl);
      return;
    }

    // ==================== Transform XML to PDF using FOP ====================//
    File pdfTempFile = transformXmlToPdf(xmlData);
    // if pdfTempFile is null for some reason, return
    if (pdfTempFile == null) {
      response.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
      logger.severe("pdfTempFile = null! for docId: " + docId);
      return;
    }

    // =================== Upload PDF file to Google Storage ===================//
    String downloadUrl = uploadPdf(docId, pdfTempFile.getAbsolutePath());

    // Done! Send url back to the user
    logger.info("Successfully transformed XML to PDF and stored it in Cloud Storage, "
        + "the pdf download url is also stored in Firebase database! DocId:" + docId);
    writer.write(downloadUrl);
  }

  // ================== Transfer XML to PDF function ==================//
  private File transformXmlToPdf(String xmlStr) throws IOException {
    // Setup input for XSLT transformation
    StringReader strReader = new StringReader(xmlStr);
    StreamSource xmlInput = new StreamSource(strReader);

    // Prepare output temp directory
    String tDir = System.getProperty("java.io.tmpdir");
    String path = tDir + "/tmp" + ".pdf";
    File pdfTempFile = new File(path);
    pdfTempFile.deleteOnExit();

    // Setup output PDF file
    OutputStream pdfOutputStream = new FileOutputStream(pdfTempFile);
    pdfOutputStream = new java.io.BufferedOutputStream(pdfOutputStream);

    try {
      // Configure fopFactory as desired
      final FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
      FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

      // Construct fop with desired output format
      Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, pdfOutputStream);

      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result pdfOutput = new SAXResult(fop.getDefaultHandler());

      // Setup XSLT
      File docBookXsl = new File("fop_docbook/fo/docbook.xsl");
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StreamSource(docBookXsl));

      // Start XSLT transformation and FOP processing
      transformer.transform(xmlInput, pdfOutput);
    } catch (FOPException | TransformerException e) {
      logger.severe("Error in xml to pdf transformation using FOP: " + e.getMessage());
      e.printStackTrace();
      return null;
    } finally {
      pdfOutputStream.close();
    }
    return pdfTempFile;
  }

  // ================= Upload PDF to storage and DB function =================//
  private String uploadPdf(String docId, String pdfTmpPath) throws IOException {
    // ------------------- Upload pdf file to Google storage -------------------//
    String objectName = "documents/" + docId + "/pdfDoc.pdf";
    Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
    BlobId blobId = BlobId.of(bucketName, objectName);
    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/pdf").build();
    Blob blob = storage.create(blobInfo, Files.readAllBytes(Paths.get(pdfTmpPath)));

    // ------------------ Set Firebase storage download token ------------------//
    // Generate unique token
    String uniqueToken = UUID.randomUUID().toString();
    // Create Firebase Storage Download Token
    Map<String, String> downloadTokens = new HashMap<>();
    downloadTokens.put("firebaseStorageDownloadTokens", uniqueToken);
    blob.toBuilder().setMetadata(downloadTokens).build().update();

    // ------------- Save The url of the pdf in firebase database -------------//
    // Get current timestamp in milliseconds
    long TimestampInMs = (Timestamp.now().getSeconds() * 1000) + (Timestamp.now().getNanos() / 1000000);
    // Get download url
    String downloadUrl = "https://firebasestorage.googleapis.com/v0/b/docbook-2021.appspot.com/o/documents%2F" + docId
        + "%2FpdfDoc.pdf?alt=media&token=" + uniqueToken;
    // Set pdfUrl and pdfUpdatedAt children
    Map<String, Object> pdfChildren = new HashMap<>();
    pdfChildren.put("pdfUrl", downloadUrl);
    pdfChildren.put("pdfUpdatedAt", TimestampInMs);
    docsRef.child(docId).child("metadata").updateChildrenAsync(pdfChildren);
    // return download url
    return downloadUrl;
  }
}