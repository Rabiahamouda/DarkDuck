(function () {
    var firebaseConfig = {
        apiKey: "AIzaSyCaaBXjrgPKRry7SU2CqD9MQ-PAuWnvoi4",
        authDomain: "docbook-2021.firebaseapp.com",
        databaseURL: "https://docbook-2021-default-rtdb.firebaseio.com",
        projectId: "docbook-2021",
        storageBucket: "docbook-2021.appspot.com",
        messagingSenderId: "438833982715",
        appId: "1:438833982715:web:0bc7de4c296d4bea5cd45e",
        measurementId: "G-S5NMEZDF65"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();

    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) {
            //hide go to dockBook editor button
            var btn = document.getElementById("goToMyProjectsBtn");
            btn.style.display = "none"
            //firebase ui stuff
            var ui = new firebaseui.auth.AuthUI(firebase.auth());
            var uiConfig = {
                callbacks: {
                    signInSuccessWithAuthResult: function (authResult, redirectUrl) {
                        // User successfully signed in.
                        // Return type determines whether we continue the redirect automatically
                        // or whether we leave that to developer to handle.
                        
                        //to update the name of the user!
                        firebase.database().ref("users").child(authResult.user.uid).child("userInfo").update({
                            name: authResult.user.displayName
                        }).then(() => {
                            window.location.replace("myProjects.html");
                        }).catch(err =>{
                            window.location.replace("myProjects.html");
                        });
                        //return false;
                    },
                    uiShown: function () {
                        // The widget is rendered.
                        // Hide the loader.
                        //document.getElementById('loader').style.display = 'none';
                    }
                },
                // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
                credentialHelper: firebaseui.auth.CredentialHelper.NONE,
                signInFlow: 'popup',
                signInSuccessUrl: 'myProjects.html',
                signInOptions: [
                    // Leave the lines as is for the providers you want to offer your users.
                    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    firebase.auth.EmailAuthProvider.PROVIDER_ID
                ]/*,
                // Terms of service url.
                tosUrl: '<your-tos-url>',
                // Privacy policy url.
                privacyPolicyUrl: '<your-privacy-policy-url>'*/
            };
            ui.start('#firebaseui-auth-container', uiConfig);
        }
    });
})();