//========= variables =========//
let docId;
let firepad;
let aceEditor;
let docsRef;
let usersRef;
let templateRef;
let templatesObject;
let membersRoleObj = {};
//roles
const OWNER = "Owner";
const EDITOR = "Editor";
const VIEWER = "Viewer";

//====== invoke on load ======//
(function () {
    //get hash, if no hash, return
    if (!window.location.hash) {
        console.log("No docId found in the hash!");
        return
    }
    docId = window.location.hash.substring(1);
    if (docId === undefined || docId === null || docId === "")
        return;

    //firebase init
    const firebaseConfig = {
        apiKey: "AIzaSyCaaBXjrgPKRry7SU2CqD9MQ-PAuWnvoi4",
        authDomain: "docbook-2021.firebaseapp.com",
        databaseURL: "https://docbook-2021-default-rtdb.firebaseio.com",
        projectId: "docbook-2021",
        storageBucket: "docbook-2021.appspot.com",
        messagingSenderId: "438833982715",
        appId: "1:438833982715:web:0bc7de4c296d4bea5cd45e",
        measurementId: "G-S5NMEZDF65"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    docsRef = firebase.database().ref("documents");
    usersRef = firebase.database().ref("users");
    templateRef = firebase.database().ref("templates");

    //the user should be signed in, or return
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;

        //====================== setup bar ====================== //
        //set doc title
        docsRef.child(docId).child("metadata/title").get().then(docTitle => {
            document.getElementById("documentTitle").innerHTML = docTitle.val();
            document.getElementById("documentTitle-mb").innerHTML = docTitle.val();
        });

        //listen to docTitle change (when press enter)
        $("#documentTitle").keypress(function (e) {
            if (e.which == 13) {
                const newDocTitle = document.getElementById("documentTitle").textContent.trim();
                document.getElementById("documentTitle").innerHTML = newDocTitle;
                document.getElementById("documentTitle-mb").innerHTML = newDocTitle;
                if (newDocTitle !== undefined && newDocTitle !== null && newDocTitle !== "")
                    docsRef.child(docId).child("metadata/title")
                        .set(newDocTitle).catch(err => console.log(err));
                aceEditor.focus();
                return false;
            }
            else return true;
        });
        //listen to docTitle change (when press enter) [MOBILE VERSION]
        $("#documentTitle-mb").keypress(function (e) {
            if (e.which == 13) {
                const newDocTitle = document.getElementById("documentTitle-mb").textContent.trim();
                document.getElementById("documentTitle-mb").innerHTML = newDocTitle;
                document.getElementById("documentTitle").innerHTML = newDocTitle;
                if (newDocTitle !== undefined && newDocTitle !== null && newDocTitle !== "")
                    docsRef.child(docId).child("metadata/title")
                        .set(newDocTitle).catch(err => console.log(err));
                aceEditor.focus();
                return false;
            }
            else return true;
        });

        //set current username and profile pic
        const profilePic = user.photoURL;
        //set username
        $('#username-bar').text(user.displayName);
        //set profilePic
        if (profilePic !== undefined && profilePic !== null && profilePic !== "") {
            //manipulate
            $("#profilePic-bar").attr("src", "");
            $("#profilePic-bar").attr("src", profilePic);
            document.getElementById("profilePic-bar").style.height = '100%';
            document.getElementById("profilePic-bar").style.width = '100%';
        }

        //customize file menu depending on user's role
        docsRef.child(docId).child("users").child(user.uid)
            .child("role").get().then(userRol => {
                // ------------------- viewer ------------------- //
                if (userRol.val() === VIEWER) {
                    document.getElementById("inviteBtn").style.display = "none";
                    document.getElementById("remove-doc-item").style.display = "none";
                    document.getElementById("remove-doc-item-mb").style.display = "none";
                    document.getElementById("rename-doc-item").style.display = "none";
                    document.getElementById("rename-doc-item-mb").style.display = "none";
                    document.getElementById("share-doc-item").style.display = "none";
                    document.getElementById("share-doc-item-mb").style.display = "none";
                    //disable editors
                    aceEditor.setReadOnly(true);
                    tinymce.activeEditor.mode.set("readonly");

                    //for users and permissions modal
                    document.getElementById("users-and-perms-item").addEventListener("click",
                        onItemClick => {
                            $('#permissionsModal').modal('show');
                            document.getElementById("perm-change-btn").style.display = "none";
                            document.getElementById("perm-cancel-btn").innerHTML = "Ok";
                            viewMembersPerm(true);
                        });

                    // ------------------- editor ------------------- //
                } else if (userRol.val() === EDITOR) {
                    document.getElementById("remove-doc-item").style.display = "none";
                    document.getElementById("remove-doc-item-mb").style.display = "none";
                    //for users and permissions modal
                    document.getElementById("users-and-perms-item").addEventListener("click",
                        onItemClick => { $('#permissionsModal').modal('show'); viewMembersPerm(false); });

                    // ------------------- owner ------------------- //
                } else { // owner
                    //for users and permissions modal
                    document.getElementById("users-and-perms-item").addEventListener("click",
                        onItemClick => { $('#permissionsModal').modal('show'); viewMembersPerm(false); });
                }
            });
        //edit tools on click actions
        editToolsOnclick();

        //====================== setup editors ====================== //
        //init tiny editor
        tinymce.init({
            selector: '#preview',
            resize: false,
            height: "100%",
            branding: false,
            menubar: false,
        });

        //init ACE editor
        aceEditor = ace.edit("firepad-container");
        aceEditor.setTheme("ace/theme/eclipse");
        aceEditor.$blockScrolling = Infinity
        const session = aceEditor.getSession();
        session.setUseWrapMode(true);
        session.setUseWorker(false);
        session.setMode("ace/mode/xml");

        //firepad init and xmlData child update
        firepadInit(user);

        //listen to htmlData (the result from transformation) and set it in tiny editor
        docsRef.child(docId + '/metadata/htmlData').on('value', (snapshot) => {
            tinymce.get("preview").setContent(snapshot.val());
        });

        //something

        //get templates
        templateRef.get().then(templatesSnaps => {
            //get all temples titles
            templatesObject = templatesSnaps.val();
            const templesKeys = Object.keys(templatesObject);
            for (let templateTitle of templesKeys) {
                const templateOption = $("<option value=\"" + templateTitle + "\">" + templateTitle + "</option>");
                templateOption.appendTo('#temples-select');
            }

            //now we can listen to create new project button click
            document.getElementById("create-new-project").addEventListener("click", createNewDoc);
        }).catch(err => {
            console.log("Failed to get templates! " + err);
        });
    });
})();

//====================== firepad init and xmlData child update ======================//
function firepadInit(user) {
    docsRef.child(docId).child("metadata/xmlData").get().then((xmlData) => {
        // Create Firepad.
        firepad = Firepad.fromACE(docsRef.child(docId), aceEditor, {
            userId: user.uid,
            defaultText: xmlData.val()
        });

        //wait a specific time (2sec) after the user releases a button, and store doc in xmlData
        firepad.on('ready', function () {
            const firepadContainer = document.getElementById("firepad-container");
            firepadContainer.addEventListener("keyup", debounce(function () {
                docsRef.child(docId + '/metadata').update({
                    xmlData: firepad.getText(),
                    xmlUpdatedBy: user.uid,
                    xmlUpdatedAt: firebase.database.ServerValue.TIMESTAMP
                });
            }, 2000)
            );
        });
    });
}

//================= debounce to control when to transform to html =================//
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

//============================= transform xml to pdf =============================//
function transformToPDF() {
    //check auth
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return
        //disable generate button
        document.getElementById("generate-pdf-btn").disabled = true;
        document.getElementById("generate-pdf-txt").innerHTML = "Generating...";

        //start
        console.log("Started xml-to-pdf transformation");
        user.getIdToken(true).then(function (tokenId) {
            fetch('https://us-central1-docbook-2021.cloudfunctions.net/xmlToPdf', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    docId: docId,
                    tokenId: tokenId
                })
            }).then(data => {
                data.text().then(downloadURL => {
                    window.open(downloadURL);
                    console.log("Download Url: " + downloadURL);
                });
                //enable generate button
                document.getElementById("generate-pdf-btn").disabled = false;
                document.getElementById("generate-pdf-txt").innerHTML = "Generate PDF";
            }).catch(error => {
                console.log("Error in xml-to-pdf transformation function! Error: " + error);
                //enable generate button
                document.getElementById("generate-pdf-btn").disabled = false;
                document.getElementById("generate-pdf-txt").innerHTML = "Generate PDF";
            });
        }).catch(function (error) {
            console.log("Error in get user token id! Error: " + error);
            //enable generate button
            document.getElementById("generate-pdf-btn").disabled = false;
            document.getElementById("generate-pdf-txt").innerHTML = "Generate PDF";
        });
    });
}

//============================= invite user to this doc =============================//
function inviteUser() {
    //get email
    const invitedEmail = document.getElementById("invitedEmail").value.trim();
    //validation
    if (invitedEmail === undefined || invitedEmail === null || invitedEmail === "") {
        $("#inviteResLabel").text("Please specify email address!");
        document.getElementById("inviteResLabel").style.visibility = "visible";
        document.getElementById("inviteResLabel").style.color = "#AE4B20";
        return;
    }
    //get role
    const invitedRole = $('#role-select').find(":selected").text();

    //set loading label
    $("#inviteResLabel").text("Loading...");
    document.getElementById("inviteResLabel").style.visibility = "visible";
    document.getElementById("inviteResLabel").style.color = "#1EAE4E";
    document.getElementById("invite-user-btn").disabled = true; //disable invite button

    //invite cloud function call
    let invite = firebase.functions().httpsCallable('inviteUser');
    invite({
        docId: docId,
        invitedEmail: invitedEmail,
        invitedRole: invitedRole
    }).then((result) => {
        // Read result of the Cloud Function.
        const userRes = result.data.userRes;
        const detailedRes = result.data.detailedRes;
        //for dev
        console.log(detailedRes);
        //for user
        $("#inviteResLabel").text(userRes);
        document.getElementById("inviteResLabel").style.visibility = "visible";
        document.getElementById("inviteResLabel").style.color = "#1EAE4E";
        document.getElementById("invite-user-btn").disabled = false; //enable invite button
    }).catch((error) => {
        // Getting the Error details.
        const code = error.code;
        const userMsg = error.message;
        const detailedMsg = error.details;
        //for dev
        console.log('Error Code: ' + code + ', details: ' + detailedMsg);
        //for user
        $("#inviteResLabel").text("Failed to invite user!\n" + userMsg);
        document.getElementById("inviteResLabel").style.visibility = "visible";
        document.getElementById("inviteResLabel").style.color = "#AE4B20";
        document.getElementById("invite-user-btn").disabled = false; //enable invite button
    });
}

//================== users and permissions for Owner, Editor or Viewer ==================//
function viewMembersPerm(isViewer) {
    //the user should be signed in, or return
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user || docId === undefined || docId === null) return;
        docsRef.child(docId).child("users").get().then(membersSnap => {
            //manipulate fields
            membersObject = membersSnap.val();
            const membersKeys = Object.keys(membersObject);
            $("#users-and-perm-div").empty();
            for (let memberId of membersKeys) {
                const user = {
                    id: membersObject[memberId].uid,
                    name: membersObject[memberId].name,
                    email: membersObject[memberId].email,
                    role: membersObject[memberId].role,
                    profilePic: membersObject[memberId].profilePic
                }
                if (isViewer) addUserDivForV(user);
                else {
                    addUserDivForOnr(user);
                    membersRoleObj[user.id] = user
                }
            }
        }).catch(err => {
            console.log(err);
        });
    });
}

//==================== change permission action if Owner or Editor ====================//
function permChangeAction() {
    //the user should be signed in, or return
    firebase.auth().onAuthStateChanged(function (currentUser) {
        if (!currentUser || docId === undefined || docId === null) return;

        const promisesArr = []; //change role promises array
        const msgsArr = []; //string of messages either success or failed
        const membersKeys = Object.keys(membersRoleObj);

        //disable change button
        document.getElementById("perm-change-btn").innerHTML = "Changing..."
        document.getElementById("perm-change-btn").disabled = true;
        //iterate through all members and do the needed action for each
        for (let memberId of membersKeys) {
            const memOldRole = membersRoleObj[memberId].role;
            const memNewRole = $('#' + memberId).find(":selected").text();

            //----------------- If member permission was not changed -----------------//
            if (memNewRole === memOldRole) continue;

            //------------------- If the member should be removed -------------------//
            if (memNewRole === "Remove") {
                let removeMember = firebase.functions().httpsCallable('removeMember');
                promisesArr.push(removeMember({
                    docId: docId,
                    memberEmail: membersRoleObj[memberId].email
                }).then((result) => {
                    // Read result of the Cloud Function.
                    const userRes = result.data.userRes;
                    const detailedRes = result.data.detailedRes;
                    //for dev
                    console.log(detailedRes);
                    //for user
                    msgsArr.push(membersRoleObj[memberId].name + ": " + userRes + "\n");
                }).catch((error) => {
                    // Getting the Error details.
                    const code = error.code;
                    const userMsg = error.message;
                    const detailedMsg = error.details;
                    //for dev
                    console.log('Error Code: ' + code + ', details: ' + detailedMsg);
                    //for user
                    msgsArr.push(membersRoleObj[memberId].name + ": " + userMsg + "\n");
                }));
                continue;
            }

            //------------- If the role of the member should be changed -------------//
            let changeRole = firebase.functions().httpsCallable('changeMemberRole');
            promisesArr.push(changeRole({
                docId: docId,
                memberEmail: membersRoleObj[memberId].email,
                memberNewRole: memNewRole
            }).then((result) => {
                // Read result of the Cloud Function.
                const userRes = result.data.userRes;
                const detailedRes = result.data.detailedRes;
                //for dev
                console.log(detailedRes);
                //for user
                msgsArr.push(membersRoleObj[memberId].name + ": " + userRes + "\n");
            }).catch((error) => {
                // Getting the Error details.
                const code = error.code;
                const userMsg = error.message;
                const detailedMsg = error.details;
                //for dev
                console.log('Error Code: ' + code + ', details: ' + detailedMsg);
                //for user
                msgsArr.push(membersRoleObj[memberId].name + ": " + userMsg + "\n");
            }));
        }

        //handel promises and output results
        Promise.allSettled(promisesArr).then(() => {
            //back to default state
            document.getElementById("perm-change-btn").innerHTML = "Change"
            document.getElementById("perm-change-btn").disabled = false;
            //hide and show modals 
            const fullMsg = msgsArr.join();
            if (fullMsg !== undefined && fullMsg !== null && fullMsg !== "") {
                $('#permissionsModal').modal('hide');
                document.getElementById("info-modal-text").innerText = fullMsg;
                $('#infoModal').modal('show');
            }
        }).catch(err => {
            console.log("Something went wrong with Promise.allSettled for changing user roles", err);
            //back to default state
            document.getElementById("perm-change-btn").innerHTML = "Change"
            document.getElementById("perm-change-btn").disabled = false;
            //hide and show modals 
            $('#permissionsModal').modal('hide');
            document.getElementById("info-modal-text").innerText = "Something went wrong!";
            $('#infoModal').modal('show');
        });
    });
}

//================================== download html file ==================================//
function downloadHtml() {
    const htmlFileContent = tinymce.get("preview").getContent();
    const htmlFileTitle = document.getElementById("documentTitle").textContent.trim();
    if (htmlFileContent === undefined || htmlFileContent === null || htmlFileContent === "")
        return;
    const blob = new Blob([htmlFileContent],
        { type: "text/html;charset=utf-8" });
    saveAs(blob, htmlFileTitle + ".html");
}

//================================== download xml file ==================================//
function downloadXml() {
    const xmlFileContent = firepad.getText();
    const xmlFileTitle = document.getElementById("documentTitle").textContent.trim();
    if (xmlFileContent === undefined || xmlFileContent === null || xmlFileContent === "")
        return;
    const blob = new Blob([xmlFileContent],
        { type: "application/docbook+xml;charset=utf-8" });
    saveAs(blob, xmlFileTitle + ".xml");
}

//================================== remove document ==================================//
function removeDoc() {
    //check auth
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user || docsRef === undefined || docsRef === null ||
            docId === undefined || docId === null)
            return;
        docsRef.child(docId).remove().then(() => {
            window.location.replace("myProjects.html");
        }).catch(err => {
            console.log("Failed to remove document with id: " + docId + ". Error: " + err);
        });
    });
}

//================================== leave document ==================================//
function leaveDoc() {
    //check auth
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return
        const myEmail = user.email;
        //validation
        if (myEmail === undefined || myEmail === null || myEmail === ""
            || docId === undefined || docId === null) return;

        //remove member cloud function call
        let removeMember = firebase.functions().httpsCallable('removeMember');
        removeMember({
            docId: docId,
            memberEmail: myEmail
        }).then((result) => {
            // Read result of the Cloud Function.
            const userRes = result.data.userRes;
            const detailedRes = result.data.detailedRes;
            //for dev
            console.log(detailedRes);
            //for user
            $('#leaveDocModal').modal('hide');
            window.location.replace("myProjects.html");
        }).catch((err) => {
            // Getting the Error details.
            const code = err.code;
            const userMsg = err.message;
            const detailedMsg = err.details;
            //for dev
            console.log('Error Code: ' + code + ', details: ' + detailedMsg);
            //for user
            $('#leaveDocModal').modal('hide');
            document.getElementById("info-modal-text").innerText = userMsg;
            $('#infoModal').modal('show');
        });
    });
}

//================================== document details ==================================//
function showDocDetails() {
    //check auth
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user || docsRef === undefined || docsRef === null ||
            docId === undefined || docId === null)
            return;
        docsRef.child(docId).child("metadata").get().then((metadataSnap) => {
            const createdAt = metadataSnap.val().createdAt;
            const createdBy = metadataSnap.val().createdBy;
            const lastModifiedAt = metadataSnap.val().xmlUpdatedAt;
            const lastModifiedBy = metadataSnap.val().xmlUpdatedBy;

            //get createdBy and updatedBy usernames 
            docsRef.child(docId).child("users/" + createdBy + "/name").get().then(createdByUsername => {
                docsRef.child(docId).child("users/" + lastModifiedBy + "/name").get().then(updatedByUsername => {
                    document.getElementById("created-label").innerHTML
                        = unixToStrDate(createdAt) + "<br />By: " + createdByUsername.val();
                    document.getElementById("modified-label").innerHTML
                        = unixToStrDate(lastModifiedAt) + "<br />By: " + updatedByUsername.val();
                }).catch(updatedByErr => {
                    console.log("Failed to get user who updated the xml for docId: "
                        + docId + ", userId: " + lastModifiedBy + ". Error: " + updatedByErr);
                });
            }).catch(createdByErr => {
                console.log("Failed to get user who created docId: " + docId +
                    ", userId: " + createdBy + ". Error: " + createdByErr);
            });
        }).catch(err => {
            console.log("Failed to get document details, docId: " + docId + ". Error: " + err);
        });
    });
}

//========================= on create new document button press =========================//
function createNewDoc() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        const docTitle = document.getElementById("newDocTitle").value.trim();
        document.getElementById("errorLabel-creatDoc").style.visibility = "hidden";
        if (docTitle === undefined || docTitle === null || docTitle === "") {
            document.getElementById("errorLabel-creatDoc").style.visibility = "visible";
            return;
        }

        //get template content and other doc details
        const selTemplateKey = $('#temples-select').find(":selected").text();
        templatesObject.None = ""; //add none to templates
        //init document info
        const docProject = {
            id: docsRef.push().key,
            title: docTitle,
            role: "Owner",
            templateContent: templatesObject[selTemplateKey]
        };
        const timeNow = firebase.database.ServerValue.TIMESTAMP;

        //add new project to firebase
        docsRef.child(docProject.id).child("metadata").set({
            docId: docProject.id,
            title: docProject.title,
            xmlData: docProject.templateContent,
            htmlData: "",
            createdAt: timeNow,
            createdBy: user.uid,
            xmlUpdatedAt: timeNow,
            xmlUpdatedBy: user.uid
        }).then(() => {
            $('#newProjectModal').modal('hide');
            document.getElementById("newDocTitle").value = "";
        }).catch(err => {
            console.log("Failed to save doc! Error: " + err);
        });

        //listen when the document will be added to user's sharedDocs
        usersRef.child(user.uid).child("sharedDocs/" + docProject.id)
            .on('value', (newDocSnap) => {
                window.location.replace("editor.html#" + docProject.id);
                location.reload();
            });
    });
}

//================================ make a copy function ================================//
function makeCopy() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) return;
        const docTitle = document.getElementById("makeCopy-docTitle").value.trim();
        document.getElementById("errorLabel-makeCopy").style.visibility = "hidden";
        if (docTitle === undefined || docTitle === null || docTitle === "") {
            document.getElementById("errorLabel-makeCopy").style.visibility = "visible";
            return;
        }

        //init document info
        const docProject = {
            id: docsRef.push().key,
            title: docTitle,
            role: "Owner",
            xmlData: firepad.getText()
        };
        const timeNow = firebase.database.ServerValue.TIMESTAMP;

        //add new project to firebase
        docsRef.child(docProject.id).child("metadata").set({
            docId: docProject.id,
            title: docProject.title,
            xmlData: docProject.xmlData,
            htmlData: "",
            createdAt: timeNow,
            createdBy: user.uid,
            xmlUpdatedAt: timeNow,
            xmlUpdatedBy: user.uid
        }).then(() => {
            $('#makeCopyModal').modal('hide');
            document.getElementById("makeCopy-docTitle").value = "";
        }).catch(err => {
            console.log("Failed to save doc! Error: " + err);
        });

        //listen when the document will be added to user's sharedDocs
        usersRef.child(user.uid).child("sharedDocs/" + docProject.id)
            .on('value', (newDocSnap) => {
                window.location.replace("editor.html#" + docProject.id);
                location.reload();
            });
    });
}

//============================= edit tools on click actions =============================//
function editToolsOnclick() {
    const copyToClipboard = str => {
        const el = document.createElement('textarea');
        el.value = str;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };
    //================== undo ==================//
    $('#undoBtn, #undo-icon-btn').click(function (e) {
        aceEditor.undo();
    });
    //================== redo ==================//
    $('#redoBtn, #redo-icon-btn').click(function (e) {
        aceEditor.redo();
    });
    //================== cut ==================//
    $('#cutBtn, #cut-icon-btn').click(function (e) {
        copyToClipboard(aceEditor.getCopyText());
        aceEditor.remove();
    });
    //================== copy ==================//
    $('#copyBtn, #copy-icon-btn').click(function (e) {
        copyToClipboard(aceEditor.getCopyText());
    });
    //================== paste ==================//
    $('#pasteBtn, #paste-icon-btn').click(function (e) {
        navigator.clipboard.readText().then(text => {
            aceEditor.execCommand("paste", text);
        }).catch(err => {
            console.error('Failed to read clipboard contents: ', err);
        });
    });
    //================== select all ==================//
    $('#selectAllBtn').click(function (e) {
        aceEditor.selectAll();
    });
}
//============================= logout function =============================//
function logout() {
    firebase.auth().signOut().then(() => {
        console.log("User logged out!");
        $('#logoutModal').modal('hide');
        window.location.replace("index.html");
    }).catch((error) => {
        console.log(error);
    });
}

//================ convert unix date to local readable date ================//
function unixToStrDate(UNIX_timestamp) {
    const utcDate = new Date(UNIX_timestamp);
    const localDate = new Date(utcDate.getTime());

    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const year = localDate.getFullYear();
    const month = months[localDate.getMonth()];
    const hour = localDate.getHours() < 10 ? '0' + localDate.getHours() : localDate.getHours();
    const min = localDate.getMinutes() < 10 ? '0' + localDate.getMinutes() : localDate.getMinutes();
    //const sec = localDate.getSeconds() < 10 ? '0' + localDate.getSeconds() : localDate.getSeconds();
    const time = localDate.getDate() + ' ' + month + ' ' + year + ' ' + hour + ':' + min;
    return time;
}

function addUserDivForOnr(user) {
    //user's profile picture settings
    if (user.profilePic === undefined || user.profilePic === null || user.profilePic == "") {
        user.profilePic = "assets/icons/profile-blue.svg";
        user.style = "width:50%; height:50%;";
    } else user.style = "width:100%; height:100%;";

    const userItem = $("<div class=\"d-flex justify-content-between align-items-center pb-3\">\n"
        + "                            <div class=\"user-info d-flex align-items-center\">\n"
        + "                                <span style=\"border: 1px solid #2E226D; height: 45px; width: 45px;\"\n"
        + "                                    class=\"circle-perm bg-white d-flex justify-content-center align-items-center\">\n"
        + "                                    <img src=\"" + user.profilePic + "\"\n"
        + "                                        style=\"" + user.style + " object-fit: cover;\">\n"
        + "                                </span>\n"
        + "                                <div class=\"username\">\n"
        + "                                    <p class=\"m-0\">" + user.name + "</p>\n"
        + "                                    <p class=\"m-0\">" + user.email + "</p>\n"
        + "                                </div>\n"
        + "                            </div>\n"
        + "                            <div class=\"dropdown\">\n"
        + "                                <select class=\"form-select dropdown btn butn-icon-pop\" id=\"" + user.id + "\">\n"
        + "                                    <option value=\"Viewer\">Viewer</option>\n"
        + "                                    <option value=\"Editor\">Editor</option>\n"
        + "                                    <option value=\"Owner\">Owner</option>\n"
        + "                                    <option value=\"Remove\">Remove</option>\n"
        + "                                </select>\n"
        + "                            </div>\n"
        + "                        </div>");

    //append it to div and select user role
    userItem.prependTo('#users-and-perm-div');
    document.getElementById(user.id).value = user.role;
}

function addUserDivForV(user) {
    //user's profile picture settings
    if (user.profilePic === undefined || user.profilePic === null || user.profilePic == "") {
        user.profilePic = "assets/icons/profile-blue.svg";
        user.style = "width:50%; height:50%;";
    } else user.style = "width:100%; height:100%;";

    const userItem = $("<div class=\"d-flex justify-content-between align-items-center pb-3\">\n"
        + "                            <div class=\"user-info d-flex align-items-center\">\n"
        + "                                <span style=\"border: 1px solid #2E226D; height: 45px; width: 45px;\"\n"
        + "                                    class=\"circle-perm bg-white d-flex justify-content-center align-items-center\">\n"
        + "                                    <img src=\"" + user.profilePic + "\"\n"
        + "                                        style=\"" + user.style + " object-fit: cover;\">\n"
        + "                                </span>\n"
        + "                                <div class=\"username\">\n"
        + "                                    <p class=\"m-0\">" + user.name + "</p>\n"
        + "                                    <p class=\"m-0\">" + user.email + "</p>\n"
        + "                                </div>\n"
        + "                            </div>\n"
        + "                            <label> " + user.role + "</label>\n"
        + "                        </div>");

    //append it to div 
    userItem.prependTo('#users-and-perm-div');
}