import * as admin from 'firebase-admin';
admin.initializeApp();

//===================== Database Trigger Functions =====================//
//trigger on create new user in auth section
export { onAuthCreateUser } from "./onAuthCreateUser";

//trigger on remove user from auth section
export { onAuthRemoveUser } from "./onAuthRemoveUser";

//trigger on update name, email or profilePic
export { onUpdateUserInfo } from "./onUpdateUserInfo";

//trigger on add document by auth user
export { onCreateDoc } from "./onCreateDoc";

//trigger on remove document by owner
export { onRemoveDoc } from "./onRemoveDoc";

//trigger on update document title
export { onUpdateDocTitle } from "./onUpdateDocTitle";

//========================== HTTP Functions ==========================//
//http function to invite new user to a doc
export { inviteUser } from "./inviteUser";

//http function to remove a member from a doc
export { removeMember } from "./removeMember";

//http function to change user's role in a doc
export { changeMemberRole } from "./changeMemberRole";